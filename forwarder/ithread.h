#ifndef ITHREAD_H
#define ITHREAD_H

#include "icommon.h"
#include <pthread.h>

typedef struct IThread IThread;
/**
 * @brief Every IThread object refers to every TCP or UDP connection
 * _FunctionToCall is externally mapped function which runs in a thread when
 * CreateAndRun called
 */
struct IThread {
    unsigned long oid;
    IBOOL isAllowed;

    pthread_t thread;
    pthread_attr_t attr;

    void* (*_FunctionToCall)(void* param);
    IBOOL (*CreateAndRun)
    (IThread* self, void* param);
    IBOOL (*Join)
    (IThread* self);
};

IBOOL InitializeIThread(IThread* iThread, void* functionToCall);

#endif
