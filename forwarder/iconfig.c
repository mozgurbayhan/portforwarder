#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "icommon.h"
#include "iconfig.h"
#include "ilogger.h"

/**
 * @brief creates a config item per line in config file
 * @param line
 * @param configItem
 * @return
 */
int _ICGetIConfigItemFromLine(char* line, IConfigItem* configItem)
{
    int i;
    int blockCounter = 0, blockSize = 0;
    int maxBlockCount = 5;
    int maxBlockSizeWZ = 26;
    int maxChar = strlen(line);
    char blocks[maxBlockCount][maxBlockSizeWZ];
    char wc = 0;
    char emptyBlock[maxBlockSizeWZ];
    IBOOL commentLine = ITRUE;
    IBOOL retval = ITRUE;
    IBOOL isEmptyLine = ITRUE;

    // cleanup
    bzero(emptyBlock, maxBlockSizeWZ);
    for (i = 0; i < maxBlockCount; i++) {
        bzero(blocks[i], maxBlockSizeWZ);
    }
    // check if comment line or empty
    if (line[0] == '\n' || line[0] == '#')
        return IFALSE;

    for (i = 0; i < maxChar; i++) {
        if (line[i] != ' ') {
            isEmptyLine = IFALSE;
            break;
        }
    }
    if (isEmptyLine == ITRUE)
        return IFALSE;

    // parse blocks
    for (i = 0; i < maxChar; i++) {
        wc = line[i];
        if (wc == '#' || wc == '\n')
            break;

        if (wc != ' ') {
            if (wc == '|') {
                blockCounter++;
                blockSize = 0;

            } else if (blockCounter < maxBlockCount && blockSize < maxBlockSizeWZ - 1) {
                commentLine = IFALSE;
                blocks[blockCounter][blockSize] = line[i];
                blockSize++;
            } else
                return IERROR_MISCONFIG;
        }
    }
    // check block integrity
    if (commentLine == IFALSE) {
        for (i = 0; i < maxBlockCount; i++) {
            if (strcmp(blocks[i], emptyBlock) == 0)
                return IERROR_MISCONFIG;
        }
    } else
        retval = IFALSE;

    // create config item
    if (strcmp(blocks[0], "TCP") == 0)
        configItem->proto = ITCP;
    else if (strcmp(blocks[0], "UDP") == 0)
        configItem->proto = IUDP;
    else if (strcmp(blocks[0], "TCPUDP") == 0)
        configItem->proto = ITCPUDP;
    else
        return IERROR_MISCONFIG;

    configItem->fromAddr = malloc(strlen(blocks[1]) + 1);
    strcpy(configItem->fromAddr, blocks[1]);
    configItem->fromPort = atoi(blocks[2]);
    if (configItem->fromPort < 1)
        return IERROR_MISCONFIG;

    configItem->toAddr = malloc(strlen(blocks[3]) + 1);
    strcpy(configItem->toAddr, blocks[3]);
    configItem->toPort = atoi(blocks[4]);
    if (configItem->toPort < 1)
        return IERROR_MISCONFIG;

    return retval;
}

/**
 * @brief reads and check consistency of config file
 * @param self
 * @return IBOOL
 */
int _ICReadFile(IConfig* self)
{
    int tempRetI;
    int lineCount = 0;
    char msg[IMAX_CONFIG_LINE_LENGTH + 50];

    FILE* fp;
    IConfigItem configItem;

    char line[IMAX_CONFIG_LINE_LENGTH];

    fp = fopen(self->filepath, "rb");
    if (!fp)
        return IERROR_CANT_OPEN_FILE;

    while (fgets(line, sizeof(line), fp)) {
        lineCount++;
        tempRetI = _ICGetIConfigItemFromLine(line, &configItem);

        if (self->configItemCount >= IPOOL_SIZE) {
            bzero(msg, sizeof(msg));
            sprintf(msg, "Kural sayisi limitleri asti (Max:%d)", IPOOL_SIZE);
            iLogger.Log(IWARNING, msg);
            break;
        }

        if (tempRetI == IERROR_MISCONFIG) {
            bzero(msg, sizeof(msg));
            sprintf(msg, "Hatali konfigurasyon satiri:%d", lineCount);
            iLogger.Log(IWARNING, msg);
        } else if (tempRetI == ITRUE) {
            memcpy(&(self->configItems[self->configItemCount]), &configItem, sizeof(IConfigItem));
            self->configItemCount++;
        }
    }
    fclose(fp);
    return ITRUE;
}

/**
 * @brief public read functioın of IConfig object
 * @param self
 * @param filepath
 * @return
 */
int ICReadConfig(IConfig* self, char* filepath)
{
    int retval = ITRUE;

    self->filepath = malloc(sizeof(filepath) + 1);
    bzero(self->filepath, sizeof(filepath) + 1);
    strcpy(self->filepath, filepath);
    retval = _ICReadFile(self);
    if (retval != ITRUE)
        return retval;

    return retval;
}

IBOOL InitializeIConfig(IConfig* iConfig)
{
    iConfig->ReadConfig = ICReadConfig;
    iConfig->configItemCount = 0;
    return ITRUE;
}
