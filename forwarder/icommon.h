#ifndef ICOMMON_H
#define ICOMMON_H

// CONFIGURATION
#define IPOOL_SIZE                       400
#define ILOOP_DELAY                      10000 // in μseconds
#define IBUFFER_SIZE                     1024
#define IMAX_CLIENT_ON_PORT_TCP          8
#define IMAX_CLIENT_ON_PORT_UDP          8
#define IMAX_SOCKET_IDLE_TIME_TCP        20 // in seconds
#define IMAX_SOCKET_IDLE_TIME_UDP        20 // in seconds

#define IMAX_IP_STRING_LENGTH            16
#define IMAX_CONFIG_LINE_LENGTH          256

#define ILOGGER_PORT                     35035
#define ILOGGER_FILE_PATH                "./portforwarder.log"
#define ILOGGER_COMM_REQUEST_STATE       's'

// EXCEPTIONS
#define IERROR_CANT_CREATE_SOCKET        -11
#define IERROR_CANT_BIND_SOCKET          -12
#define IERROR_CONNECTION_FAILED         -13
#define IERROR_CANT_CHANGE_BLOCKING_MODE -14
#define IERROR_SERVER_DOESNT_EXISTS      -15
#define IERROR_CANT_STOP_SERVER          -16

#define IERROR_CANT_JOIN_THREAD          -21
#define IERROR_CANT_CREATE_THREAD        -22

#define IERROR_CANT_OPEN_FILE            -31
#define IERROR_CANT_ALLOCATE_MEMORY      -32
#define IERROR_CANT_READ_FILE            -33
#define IERROR_MISCONFIG                 -34

// CUSTOM TYPES
#define ITRUE                            1
#define IFALSE                           0
typedef int IBOOL;
typedef enum { ITCP, IUDP, ITCPUDP } IPROTO_TYPE;
typedef enum { ISERVER, ICLIENT } ISOCK_DIRECTION;
typedef enum { IERROR, IWARNING, IDEBUG } ILOG_TYPE;

unsigned long  IGetObjectId(void);

// Macros
#define SUPPRESS_UNUSED_WARN(x) (void)(x)

#endif // ifndef ICOMMON_H


