#ifndef IFORWARDER_H
#define IFORWARDER_H

#include "icommon.h"
#include "itcpsocket.h"
#include "itcpconnection.h"

typedef struct IForwarder IForwarder;
typedef struct ISession ISession;

struct ISession
{
    ITCPConnection acceptedConn;
    ITCPConnection requestedConn;
    ITCPSocket requestedSocket;
    int oid;
};

struct IForwarder
{
    ITCPSocket listenSocket;
    ITCPConnection listenConnUDP;
    ISession sessions[IMAX_CLIENT_ON_PORT];
    char *listenAddr;
    char *targetAddr;
    int listenPort, targetPort;
    int oid;
    int sessionsCount;
    IBOOL isEnabled;


    int (*Create)(IForwarder *self);
    IBOOL (*Forward)(IForwarder *self);
    IBOOL (*Destroy)(IForwarder *self);

};


IBOOL InitializeIForwarder(IForwarder *iForwarder, char *listenAddr, char *targetAddr, int listenPort, int targetPort, ICONN_TYPE connType);
#endif // ifndef IFORWARDER_H
