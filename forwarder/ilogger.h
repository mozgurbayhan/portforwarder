#ifndef ILOGGER_H
#define ILOGGER_H

#include "icommon.h"
#include "ithread.h"
#include <arpa/inet.h>

/**
 * @brief The ILogger object for logging to file, udp or screen
 */
struct ILogger {
    IBOOL isEnabled;
    IBOOL logToFile;
    IBOOL logToUdp;
    IBOOL logToConsole;

    // UDP SERVER
    IBOOL isUDPAllowed;
    IBOOL sessionsRequested;
    IBOOL hasConnection;
    int sock;

    IThread serverThread;
    struct sockaddr_in si_other;

    // -UDP SERVER

    IBOOL (*Log)
    (ILOG_TYPE logType, char* msg);
    IBOOL (*Destroy)
    ();

} iLogger;

IBOOL InitializeILogger(IBOOL logToFile, IBOOL logToUdp, IBOOL logToConsole);

#endif // ifndef ILOGGER_H
