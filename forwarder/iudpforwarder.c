#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "icommon.h"
#include "ilogger.h"
#include "iudpforwarder.h"
#include "iudpsocket.h"

/**
 * @brief _IUDPFDeleteIForwardObject deletes requestedSocket
 * @param self
 * @param index
 * @return
 */
IBOOL _IUDPFDeleteIForwardObject(IUDPForwarder* self, int index)
{
    IUDPSession dumbSession;
    IUDPSocket* rsock = &(self->sessions[index].requestedSocket);
    int cursor;

    rsock->Destroy(rsock);
    self->sessions[index] = dumbSession;
    if (index + 1 < self->sessionsCount) {
        for (cursor = index; cursor < self->sessionsCount - 1; cursor++) {
            self->sessions[cursor] = self->sessions[cursor + 1];
            self->sessions[cursor + 1] = dumbSession;
        }
    }
    self->sessionsCount--;
    return ITRUE;
}
/**
 * @brief _IUDPFAddSession adss session to IUDPForwarder
 * @param self
 * @return
 */
IBOOL _IUDPFAddSession(IUDPForwarder* self)
{
    IUDPSession* sess = &(self->sessions[self->sessionsCount]);

    memcpy(&(sess->acceptedAddress), &(self->listenSocket.sockAddrRemote), sizeof(struct sockaddr_in));
    if (InitializeIUDPSocket(&(sess->requestedSocket), self->targetAddr, self->targetPort, ICLIENT) == IFALSE)
        return IFALSE;
    if (sess->requestedSocket.Create(&(sess->requestedSocket)) == IFALSE)
        return IFALSE;
    sess->oid = IGetObjectId();
    self->sessionsCount++;
    return ITRUE;
}

/**
 * @brief _IUDPFCompareUDPAdress compares addr1 and addr2 are same or not
 * @param addr1
 * @param addr2
 * @return
 */
IBOOL _IUDPFCompareUDPAdress(struct sockaddr_in* addr1, struct sockaddr_in* addr2)
{
    IBOOL retval = IFALSE;

    if (addr1->sin_addr.s_addr == addr2->sin_addr.s_addr && addr1->sin_port == addr2->sin_port)
        retval = ITRUE;
    return retval;
}

int IUDPFCreate(IUDPForwarder* self)
{
    InitializeIUDPSocket(&(self->listenSocket), self->listenAddr, self->listenPort, ISERVER);
    return self->listenSocket.Create(&(self->listenSocket));
}

/**
 * @brief ITCPFForward forwards incoming data to target connection
 * @param self
 * @return
 */
IBOOL IUDPFForward(IUDPForwarder* self)
{
    IBOOL tempRetB;
    IBOOL inList = IFALSE;
    time_t now;
    int i;
    IUDPSocket* ls = &(self->listenSocket);
    IUDPSession* iso;
    IUDPSocket* rsock;
    char msg[256] = { 0 };

    tempRetB = ls->Recieve(ls);
    if (tempRetB == ITRUE) {
        for (i = 0; i < self->sessionsCount; i++) {
            inList = _IUDPFCompareUDPAdress(&(ls->sockAddrRemote), &(self->sessions[i].acceptedAddress));
            if (inList == ITRUE) {
                rsock = &(self->sessions[i].requestedSocket);
                rsock->SendTo(rsock, ls->buffer, ls->bufferDataSize);
                ls->ClearData(ls);
                break;
            }
        }

        if (inList == IFALSE && self->sessionsCount < IMAX_CLIENT_ON_PORT_UDP) {

            sprintf(msg, "NEW UDP -> L: %s:%d | R: %s:%d", self->listenAddr, self->listenPort, inet_ntoa(ls->sockAddrRemote.sin_addr), (int)ntohs(ls->sockAddrRemote.sin_port));
            iLogger.Log(IDEBUG, msg);
            _IUDPFAddSession(self);
            rsock = &(self->sessions[self->sessionsCount - 1].requestedSocket);
            rsock->SendTo(rsock, ls->buffer, ls->bufferDataSize);
            ls->ClearData(ls);
        }
    }
    for (i = 0; i < self->sessionsCount; i++) {
        tempRetB = self->sessions[i].requestedSocket.Recieve(&(self->sessions[i].requestedSocket));
        if (tempRetB == ITRUE) {
            memcpy(&(self->listenSocket.sockAddrRemote), &(self->sessions[i].acceptedAddress), sizeof(struct sockaddr_in));
            self->listenSocket.SendTo(&(self->listenSocket), self->sessions[i].requestedSocket.buffer, self->sessions[i].requestedSocket.bufferDataSize);
            self->sessions[i].requestedSocket.ClearData(&(self->sessions[i].requestedSocket));
        }
    }

    // Cleanup
    time(&now);
    for (i = self->sessionsCount - 1; i > -1; i--) {
        iso = &(self->sessions[i]);

        if (
            difftime(now, iso->requestedSocket.lastDataTime) > IMAX_SOCKET_IDLE_TIME_UDP && difftime(now, iso->lastAcceptedDataTime) > IMAX_SOCKET_IDLE_TIME_UDP) {
            bzero(msg, 256);
            sprintf(msg, "DEL UDP -> L: %s:%d | R: %s:%d", self->listenAddr, self->listenPort, inet_ntoa(iso->acceptedAddress.sin_addr), (int)ntohs(iso->acceptedAddress.sin_port));
            iLogger.Log(IDEBUG, msg);
            _IUDPFDeleteIForwardObject(self, i);
        }
    }

    return ITRUE;
}
IBOOL IUDPFDestroy(IUDPForwarder* self)
{
    int i;

    //    IBOOL tmpVal;

    for (i = self->sessionsCount - 1; i > -1; i--) {
        _IUDPFDeleteIForwardObject(self, i);
    }
    self->listenSocket.Destroy(&(self->listenSocket));

    free(self->listenAddr);
    free(self->targetAddr);

    return ITRUE;
}

IBOOL InitializeIUDPForwarder(IUDPForwarder* iUDPForwarder, char* listenAddr, char* targetAddr, int listenPort, int targetPort)
{
    iUDPForwarder->oid = IGetObjectId();
    iUDPForwarder->sessionsCount = 0;
    iUDPForwarder->isEnabled = ITRUE;

    iUDPForwarder->listenAddr = malloc(sizeof(listenAddr));
    iUDPForwarder->targetAddr = malloc(sizeof(targetAddr));
    strcpy(iUDPForwarder->listenAddr, listenAddr);
    strcpy(iUDPForwarder->targetAddr, targetAddr);
    iUDPForwarder->listenPort = listenPort;
    iUDPForwarder->targetPort = targetPort;

    iUDPForwarder->Create = IUDPFCreate;
    iUDPForwarder->Forward = IUDPFForward;
    iUDPForwarder->Destroy = IUDPFDestroy;
    return ITRUE;
}
