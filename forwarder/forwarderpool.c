#include "forwarderpool.h"
#include "icommon.h"
#include "ithread.h"
#include <unistd.h>

/**
 * @brief removes and destroys ForwarderPoolObject from pool at given index
 * @param self
 * @param index
 * @return
 */
IBOOL FPRemove(ForwarderPool* self, int index)
{
    int cursor;

    self->pool[index].iTCPForwarder.Destroy(&(self->pool[index].iTCPForwarder));
    self->pool[index].iUDPForwarder.Destroy(&(self->pool[index].iUDPForwarder));
    self->pool[index].iThread.Join(&(self->pool[index].iThread));

    if (index + 1 < self->poolSize) {
        for (cursor = index; cursor < self->poolSize - 1; cursor++) {
            self->pool[cursor] = self->pool[cursor + 1];
        }
    }
    self->poolSize--;

    return ITRUE;
}
/**
 * @brief Removes and destroys all objects in the pool
 * @param self
 * @return
 */
IBOOL FPRemoveAll(ForwarderPool* self)
{
    int i;
    for (i = 0; i < self->poolSize; i++) {
        self->pool[i].iThread.isAllowed = IFALSE;
    }

    usleep(ILOOP_DELAY * 2);

    for (i = self->poolSize; i > -1; i--) {
        self->Remove(self, i);
    }

    return ITRUE;
}

IBOOL InitializeForwarderPool(ForwarderPool* forwarderPool)
{
    forwarderPool->poolSize = 0;

    forwarderPool->Remove = FPRemove;
    forwarderPool->RemoveAll = FPRemoveAll;
    return ITRUE;
}
