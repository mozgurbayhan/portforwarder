#ifndef ITCPSOCKET_H
#define ITCPSOCKET_H
#include <netinet/in.h>
#include <time.h>
#include "icommon.h"
#include "ithread.h"

typedef struct ITCPSocket ITCPSocket;
typedef struct ITCPConnection ITCPConnection;


//////// ITCPConnection ////////
/**
 * @brief The ITCPConnection object holds incoming or outgoing connection
 */
struct ITCPConnection
{
    IBOOL isInitialized;
    IBOOL isAlive;
    ITCPSocket *parentSocket;

    struct sockaddr_in sockAddrFrom;
    struct sockaddr_in sockAddrTo;
    char buffer[IBUFFER_SIZE];
    int bufferDataSize;
    int conn;
    unsigned long  oid;
    time_t lastDataTime;


    IBOOL (*Send)(ITCPConnection *self, char *data, int dataSize);
    IBOOL (*Receive)(ITCPConnection *self);
    IBOOL (*ClearData)(ITCPConnection *self);
    IBOOL (*Close)(ITCPConnection *self);

};

IBOOL InitializeITCPConnection(ITCPConnection *iTCPConnection, ITCPSocket *iTCPSocket, int conn, struct sockaddr_in *sockAddrFrom, struct sockaddr_in *sockAddrTo);

//////// ITCPSocket ////////
/**
 * @brief The ITCPSocket object holds server or client socket info
 */
struct ITCPSocket
{
    IBOOL isInitialized;
    IBOOL isAlive;
    char addrIP[IMAX_IP_STRING_LENGTH];
    int port;
    ISOCK_DIRECTION sockDirection;
    int oid;

    IThread _listenThread;
    ITCPConnection _acceptedConnection;
    IBOOL _hasAcceptedConnection;
    IBOOL hasPlaceToAccept;

    struct sockaddr_in sockAddrDirEnd;
    int sock;

    int (*Create)(ITCPSocket *self);
    IBOOL(*Destroy)(ITCPSocket *self);
    IBOOL (*GetNewConnection)(ITCPSocket *self, ITCPConnection *uninitializedConnection, IBOOL continueListenAfter);

};

IBOOL InitializeITCPSocket(ITCPSocket *iTCPSocket, char *addr, int port, ISOCK_DIRECTION sockDirection);

#endif // ifndef ISOCKET_H
