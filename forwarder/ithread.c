#include "ithread.h"
#include "icommon.h"
#include "ilogger.h"
#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>

/**
 * @brief ITCreateAndRun stars new posix thread by calling _FunctionToCall
 *  which is defined previously
 * @param self
 * @param param
 * @return
 */
IBOOL ITCreateAndRun(IThread* self, void* param)
{

    char msg[256] = { 0 };
    IBOOL retval;
    int rv;

    // set thread detachstate attribute to DETACHED
    pthread_attr_init(&self->attr);
    pthread_attr_setdetachstate(&self->attr, PTHREAD_CREATE_DETACHED);

    rv = pthread_create(&(self->thread), &self->attr, self->_FunctionToCall, param);
    //-FH

    //    rv = pthread_create(&(self->thread), 0, self->_FunctionToCall, param);
    if (rv == 0)
        retval = ITRUE;

    else {

        sprintf(msg, "Thread olusturulamiyor <%s> ", strerror(errno));
        iLogger.Log(IERROR, msg);
        retval = IFALSE;
    }
    return retval;
}

/**
 * @brief ITJoin joşins the posix thread to main thread
 * @param self
 * @return
 */
IBOOL ITJoin(IThread* self)
{
    IBOOL retval = ITRUE;
    pthread_join(self->thread, NULL);
    return retval;
}

IBOOL InitializeIThread(IThread* iThread, void* functionToCall)
{
    iThread->oid = IGetObjectId();
    iThread->_FunctionToCall = functionToCall;
    iThread->isAllowed = ITRUE;

    iThread->CreateAndRun = ITCreateAndRun;
    iThread->Join = ITJoin;

    return ITRUE;
}
