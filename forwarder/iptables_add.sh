
#!/bin/sh
echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -F
iptables -t nat -F
iptables -X


iptables -t nat -A PREROUTING -p udp --dport 10000 -j DNAT --to-destination 192.168.10.2:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.2 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10001 -j DNAT --to-destination 192.168.10.3:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.3 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10002 -j DNAT --to-destination 192.168.10.4:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.4 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10003 -j DNAT --to-destination 192.168.10.5:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.5 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10004 -j DNAT --to-destination 192.168.10.6:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.6 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10005 -j DNAT --to-destination 192.168.10.7:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.7 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10006 -j DNAT --to-destination 192.168.10.8:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.8 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10007 -j DNAT --to-destination 192.168.10.9:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.9 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10008 -j DNAT --to-destination 192.168.10.10:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.10 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10009 -j DNAT --to-destination 192.168.10.11:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.11 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10010 -j DNAT --to-destination 192.168.10.12:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.12 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10011 -j DNAT --to-destination 192.168.10.13:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.13 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10012 -j DNAT --to-destination 192.168.10.14:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.14 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10013 -j DNAT --to-destination 192.168.10.15:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.15 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10014 -j DNAT --to-destination 192.168.10.16:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.16 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10015 -j DNAT --to-destination 192.168.10.17:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.17 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10016 -j DNAT --to-destination 192.168.10.18:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.18 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10017 -j DNAT --to-destination 192.168.10.19:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.19 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10018 -j DNAT --to-destination 192.168.10.20:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.20 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10019 -j DNAT --to-destination 192.168.10.21:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.21 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10020 -j DNAT --to-destination 192.168.10.22:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.22 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10021 -j DNAT --to-destination 192.168.10.23:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.23 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10022 -j DNAT --to-destination 192.168.10.24:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.24 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10023 -j DNAT --to-destination 192.168.10.25:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.25 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10024 -j DNAT --to-destination 192.168.10.26:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.26 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10025 -j DNAT --to-destination 192.168.10.27:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.27 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10026 -j DNAT --to-destination 192.168.10.28:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.28 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10027 -j DNAT --to-destination 192.168.10.29:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.29 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10028 -j DNAT --to-destination 192.168.10.30:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.30 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10029 -j DNAT --to-destination 192.168.10.31:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.31 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10030 -j DNAT --to-destination 192.168.10.32:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.32 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10031 -j DNAT --to-destination 192.168.10.33:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.33 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10032 -j DNAT --to-destination 192.168.10.34:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.34 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10033 -j DNAT --to-destination 192.168.10.35:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.35 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10034 -j DNAT --to-destination 192.168.10.36:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.36 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10035 -j DNAT --to-destination 192.168.10.37:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.37 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10036 -j DNAT --to-destination 192.168.10.38:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.38 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10037 -j DNAT --to-destination 192.168.10.39:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.39 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10038 -j DNAT --to-destination 192.168.10.40:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.40 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10039 -j DNAT --to-destination 192.168.10.41:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.41 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10040 -j DNAT --to-destination 192.168.10.42:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.42 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10041 -j DNAT --to-destination 192.168.10.43:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.43 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10042 -j DNAT --to-destination 192.168.10.44:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.44 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10043 -j DNAT --to-destination 192.168.10.45:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.45 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10044 -j DNAT --to-destination 192.168.10.46:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.46 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10045 -j DNAT --to-destination 192.168.10.47:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.47 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10046 -j DNAT --to-destination 192.168.10.48:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.48 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10047 -j DNAT --to-destination 192.168.10.49:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.49 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10048 -j DNAT --to-destination 192.168.10.50:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.50 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10049 -j DNAT --to-destination 192.168.10.51:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.51 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10050 -j DNAT --to-destination 192.168.10.52:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.52 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10051 -j DNAT --to-destination 192.168.10.53:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.53 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10052 -j DNAT --to-destination 192.168.10.54:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.54 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10053 -j DNAT --to-destination 192.168.10.55:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.55 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10054 -j DNAT --to-destination 192.168.10.56:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.56 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10055 -j DNAT --to-destination 192.168.10.57:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.57 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10056 -j DNAT --to-destination 192.168.10.58:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.58 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10057 -j DNAT --to-destination 192.168.10.59:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.59 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10058 -j DNAT --to-destination 192.168.10.60:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.60 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10059 -j DNAT --to-destination 192.168.10.61:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.61 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10060 -j DNAT --to-destination 192.168.10.62:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.62 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10061 -j DNAT --to-destination 192.168.10.63:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.63 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10062 -j DNAT --to-destination 192.168.10.64:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.64 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10063 -j DNAT --to-destination 192.168.10.65:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.65 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10064 -j DNAT --to-destination 192.168.10.66:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.66 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10065 -j DNAT --to-destination 192.168.10.67:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.67 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10066 -j DNAT --to-destination 192.168.10.68:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.68 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10067 -j DNAT --to-destination 192.168.10.69:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.69 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10068 -j DNAT --to-destination 192.168.10.70:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.70 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10069 -j DNAT --to-destination 192.168.10.71:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.71 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10070 -j DNAT --to-destination 192.168.10.72:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.72 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10071 -j DNAT --to-destination 192.168.10.73:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.73 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10072 -j DNAT --to-destination 192.168.10.74:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.74 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10073 -j DNAT --to-destination 192.168.10.75:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.75 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10074 -j DNAT --to-destination 192.168.10.76:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.76 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10075 -j DNAT --to-destination 192.168.10.77:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.77 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10076 -j DNAT --to-destination 192.168.10.78:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.78 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10077 -j DNAT --to-destination 192.168.10.79:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.79 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10078 -j DNAT --to-destination 192.168.10.80:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.80 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10079 -j DNAT --to-destination 192.168.10.81:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.81 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10080 -j DNAT --to-destination 192.168.10.82:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.82 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10081 -j DNAT --to-destination 192.168.10.83:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.83 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10082 -j DNAT --to-destination 192.168.10.84:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.84 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10083 -j DNAT --to-destination 192.168.10.85:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.85 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10084 -j DNAT --to-destination 192.168.10.86:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.86 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10085 -j DNAT --to-destination 192.168.10.87:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.87 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10086 -j DNAT --to-destination 192.168.10.88:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.88 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10087 -j DNAT --to-destination 192.168.10.89:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.89 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10088 -j DNAT --to-destination 192.168.10.90:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.90 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10089 -j DNAT --to-destination 192.168.10.91:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.91 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10090 -j DNAT --to-destination 192.168.10.92:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.92 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10091 -j DNAT --to-destination 192.168.10.93:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.93 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10092 -j DNAT --to-destination 192.168.10.94:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.94 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10093 -j DNAT --to-destination 192.168.10.95:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.95 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10094 -j DNAT --to-destination 192.168.10.96:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.96 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10095 -j DNAT --to-destination 192.168.10.97:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.97 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10096 -j DNAT --to-destination 192.168.10.98:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.98 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10097 -j DNAT --to-destination 192.168.10.99:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.99 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10098 -j DNAT --to-destination 192.168.10.100:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.100 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10099 -j DNAT --to-destination 192.168.10.250:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.250 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10100 -j DNAT --to-destination 192.168.10.102:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.102 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10101 -j DNAT --to-destination 192.168.10.103:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.103 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10102 -j DNAT --to-destination 192.168.10.104:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.104 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10103 -j DNAT --to-destination 192.168.10.105:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.105 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10104 -j DNAT --to-destination 192.168.10.106:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.106 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10105 -j DNAT --to-destination 192.168.10.107:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.107 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10106 -j DNAT --to-destination 192.168.10.108:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.108 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10107 -j DNAT --to-destination 192.168.10.109:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.109 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10108 -j DNAT --to-destination 192.168.10.110:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.110 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10109 -j DNAT --to-destination 192.168.10.111:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.111 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10110 -j DNAT --to-destination 192.168.10.112:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.112 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10111 -j DNAT --to-destination 192.168.10.113:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.113 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10112 -j DNAT --to-destination 192.168.10.114:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.114 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10113 -j DNAT --to-destination 192.168.10.115:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.115 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10114 -j DNAT --to-destination 192.168.10.116:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.116 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10115 -j DNAT --to-destination 192.168.10.117:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.117 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10116 -j DNAT --to-destination 192.168.10.118:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.118 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10117 -j DNAT --to-destination 192.168.10.119:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.119 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10118 -j DNAT --to-destination 192.168.10.120:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.120 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10119 -j DNAT --to-destination 192.168.10.121:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.121 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10120 -j DNAT --to-destination 192.168.10.122:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.122 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10121 -j DNAT --to-destination 192.168.10.123:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.123 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10122 -j DNAT --to-destination 192.168.10.124:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.124 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10123 -j DNAT --to-destination 192.168.10.125:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.125 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10124 -j DNAT --to-destination 192.168.10.126:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.126 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10125 -j DNAT --to-destination 192.168.10.127:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.127 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10126 -j DNAT --to-destination 192.168.10.128:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.128 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10127 -j DNAT --to-destination 192.168.10.129:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.129 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10128 -j DNAT --to-destination 192.168.10.130:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.130 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10129 -j DNAT --to-destination 192.168.10.131:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.131 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10130 -j DNAT --to-destination 192.168.10.132:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.132 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10131 -j DNAT --to-destination 192.168.10.133:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.133 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10132 -j DNAT --to-destination 192.168.10.134:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.134 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10133 -j DNAT --to-destination 192.168.10.135:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.135 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10134 -j DNAT --to-destination 192.168.10.136:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.136 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10135 -j DNAT --to-destination 192.168.10.137:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.137 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10136 -j DNAT --to-destination 192.168.10.138:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.138 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10137 -j DNAT --to-destination 192.168.10.139:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.139 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10138 -j DNAT --to-destination 192.168.10.140:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.140 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10139 -j DNAT --to-destination 192.168.10.141:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.141 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10140 -j DNAT --to-destination 192.168.10.142:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.142 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10141 -j DNAT --to-destination 192.168.10.143:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.143 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10142 -j DNAT --to-destination 192.168.10.144:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.144 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10143 -j DNAT --to-destination 192.168.10.145:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.145 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10144 -j DNAT --to-destination 192.168.10.146:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.146 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10145 -j DNAT --to-destination 192.168.10.147:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.147 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10146 -j DNAT --to-destination 192.168.10.148:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.148 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10147 -j DNAT --to-destination 192.168.10.149:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.149 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10148 -j DNAT --to-destination 192.168.10.150:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.150 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10149 -j DNAT --to-destination 192.168.10.151:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.151 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10150 -j DNAT --to-destination 192.168.10.152:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.152 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10151 -j DNAT --to-destination 192.168.10.153:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.153 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10152 -j DNAT --to-destination 192.168.10.154:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.154 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10153 -j DNAT --to-destination 192.168.10.155:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.155 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10154 -j DNAT --to-destination 192.168.10.156:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.156 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10155 -j DNAT --to-destination 192.168.10.157:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.157 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10156 -j DNAT --to-destination 192.168.10.158:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.158 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10157 -j DNAT --to-destination 192.168.10.159:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.159 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10158 -j DNAT --to-destination 192.168.10.160:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.160 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10159 -j DNAT --to-destination 192.168.10.161:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.161 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10160 -j DNAT --to-destination 192.168.10.162:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.162 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10161 -j DNAT --to-destination 192.168.10.163:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.163 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10162 -j DNAT --to-destination 192.168.10.164:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.164 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10163 -j DNAT --to-destination 192.168.10.165:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.165 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10164 -j DNAT --to-destination 192.168.10.166:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.166 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10165 -j DNAT --to-destination 192.168.10.167:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.167 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10166 -j DNAT --to-destination 192.168.10.168:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.168 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10167 -j DNAT --to-destination 192.168.10.169:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.169 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10168 -j DNAT --to-destination 192.168.10.170:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.170 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10169 -j DNAT --to-destination 192.168.10.171:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.171 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10170 -j DNAT --to-destination 192.168.10.172:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.172 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10171 -j DNAT --to-destination 192.168.10.173:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.173 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10172 -j DNAT --to-destination 192.168.10.174:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.174 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10173 -j DNAT --to-destination 192.168.10.175:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.175 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10174 -j DNAT --to-destination 192.168.10.176:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.176 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10175 -j DNAT --to-destination 192.168.10.177:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.177 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10176 -j DNAT --to-destination 192.168.10.178:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.178 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10177 -j DNAT --to-destination 192.168.10.179:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.179 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10178 -j DNAT --to-destination 192.168.10.180:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.180 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10179 -j DNAT --to-destination 192.168.10.181:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.181 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10180 -j DNAT --to-destination 192.168.10.182:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.182 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10181 -j DNAT --to-destination 192.168.10.183:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.183 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10182 -j DNAT --to-destination 192.168.10.184:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.184 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10183 -j DNAT --to-destination 192.168.10.185:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.185 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10184 -j DNAT --to-destination 192.168.10.186:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.186 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10185 -j DNAT --to-destination 192.168.10.187:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.187 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10186 -j DNAT --to-destination 192.168.10.188:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.188 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10187 -j DNAT --to-destination 192.168.10.189:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.189 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10188 -j DNAT --to-destination 192.168.10.190:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.190 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10189 -j DNAT --to-destination 192.168.10.191:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.191 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10190 -j DNAT --to-destination 192.168.10.192:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.192 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10191 -j DNAT --to-destination 192.168.10.193:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.193 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10192 -j DNAT --to-destination 192.168.10.194:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.194 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10193 -j DNAT --to-destination 192.168.10.195:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.195 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10194 -j DNAT --to-destination 192.168.10.196:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.196 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10195 -j DNAT --to-destination 192.168.10.197:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.197 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10196 -j DNAT --to-destination 192.168.10.198:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.198 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10197 -j DNAT --to-destination 192.168.10.199:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.199 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10198 -j DNAT --to-destination 192.168.10.200:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.200 --dport 10000 -j SNAT --to-source 192.168.10.101

iptables -t nat -A PREROUTING -p udp --dport 10199 -j DNAT --to-destination 192.168.10.201:10000
iptables -t nat -A POSTROUTING -p udp -d 192.168.10.201 --dport 10000 -j SNAT --to-source 192.168.10.101
