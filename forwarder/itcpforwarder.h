#ifndef ITCPFORWARDER_H
#define ITCPFORWARDER_H

#include "icommon.h"
#include "itcpsocket.h"

typedef struct ITCPForwarder ITCPForwarder;
typedef struct ITCPSession ITCPSession;

/**
 * @brief The ITCPSession holds accepted and requested connection info
 */
struct ITCPSession {
    ITCPConnection acceptedConn;
    ITCPConnection requestedConn;
    ITCPSocket requestedSocket;
    unsigned long oid;
};

/**
 * @brief The ITCPForwarder object is the main listener and forwarder described in config file
 */
struct ITCPForwarder {
    ITCPSocket listenSocket;
    ITCPSession sessions[IMAX_CLIENT_ON_PORT_TCP];
    char* listenAddr;
    char* targetAddr;
    int listenPort, targetPort;
    unsigned long oid;
    int sessionsCount;
    IBOOL isEnabled;

    int (*Create)(ITCPForwarder* self);
    IBOOL(*Forward)
    (ITCPForwarder* self);
    IBOOL(*Destroy)
    (ITCPForwarder* self);
};

IBOOL InitializeITCPForwarder(ITCPForwarder* iTCPForwarder, char* listenAddr, char* targetAddr, int listenPort, int targetPort);
#endif
