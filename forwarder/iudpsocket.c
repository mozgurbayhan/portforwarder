#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <string.h>

#include "icommon.h"
#include "iudpsocket.h"
#include "ilogger.h"

/**
 * @brief IUDPSCreate creates an UDP socket
 * @param self
 * @return
 */
int IUDPSCreate(IUDPSocket *self)
{
    char msg[256] = { 0 };
    // create a UDP socket
    self->sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (self->sock == -1)
    {
        sprintf(msg, "UDP soketi olusturulamiyor <%s> > %s:%d", strerror(errno), self->addrIP, self->port);
        iLogger.Log(IERROR, msg);
        return IERROR_CANT_CREATE_SOCKET;
    }

    // zero out the structure
    bzero(&(self->sockAddrLocal), sizeof(self->sockAddrLocal));
    bzero(&(self->sockAddrRemote), sizeof(self->sockAddrRemote));

    if (self->direction == ISERVER)
    {

        self->sockAddrLocal.sin_family = AF_INET;
        self->sockAddrLocal.sin_port = htons(self->port);
        self->sockAddrLocal.sin_addr.s_addr = htonl(INADDR_ANY);

        // bind socket to port
        if ( bind(self->sock, (struct sockaddr *) &(self->sockAddrLocal), sizeof(self->sockAddrLocal) ) == -1){

            sprintf(msg, "UDP sokete baglanamiyor <%s> > %s:%d", strerror(errno), self->addrIP, self->port);
            iLogger.Log(IERROR, msg);
            return IERROR_CANT_BIND_SOCKET;
        }
    }
    else if (self->direction == ICLIENT)
    {
        self->sockAddrRemote.sin_family = AF_INET;
        self->sockAddrRemote.sin_port = htons(self->port);
//        self->sockAddrLocal.sin_addr.s_addr = htonl(self->addrIP);

        if (inet_aton(self->addrIP, &(self->sockAddrLocal.sin_addr)) == 0){

            sprintf(msg, "UDP soketi dinleyemiyor <%s> > %s:%d", strerror(errno), self->addrIP, self->port);
            iLogger.Log(IERROR, msg);
            return IERROR_CANT_BIND_SOCKET;
        }

    }
    else
        return IFALSE;
    return ITRUE;
}
IBOOL IUDPSDestroy(IUDPSocket *self)
{
    IBOOL retval=ITRUE;
    char msg[256] = { 0 };
    if (close(self->sock) < 0)
    {
        sprintf(msg, "UDP soketi kapatilamiyor <%s> > %s:%d", strerror(errno), self->addrIP, self->port);
        iLogger.Log(IERROR, msg);
        retval = IFALSE;
    }
    return retval;
}
/**
 * @brief IUDPSSendTo sends data to target
 * @param self
 * @param data
 * @param dataSize
 * @return
 */
IBOOL IUDPSSendTo(IUDPSocket *self, char *data, int dataSize)
{
    int tempRetI;

    if (self->direction == ISERVER || self->direction == ICLIENT)
    {
        tempRetI = sendto(self->sock, data, dataSize, MSG_DONTWAIT, (struct sockaddr *) &self->sockAddrRemote, sizeof(self->sockAddrRemote));
        if (tempRetI == -1)
            return IFALSE;
        else
            self->lastDataTime = time(NULL);

    }
    else
        return IFALSE;
    return ITRUE;
}

/**
 * @brief IUDPSRecieve recive data from incoming socket
 * @param self
 * @return
 */
IBOOL IUDPSRecieve(IUDPSocket *self)
{

    int tempRetI;
    socklen_t remoteAddrLen = sizeof(self->sockAddrRemote);


    if (self->direction == ISERVER || self->direction == ICLIENT)
    {
        tempRetI = recvfrom(self->sock, self->buffer, IBUFFER_SIZE, MSG_DONTWAIT, (struct sockaddr *) &(self->sockAddrRemote), &remoteAddrLen);
        self->bufferDataSize = tempRetI;
        if (tempRetI < 1)
            return IFALSE;
        else
            self->lastDataTime = time(NULL);
    }
    else
        return IFALSE;
    return ITRUE;
}

/**
 * @brief IUDPSClearData clears transferred data
 * @param self
 * @return
 */
IBOOL IUDPSClearData(IUDPSocket *self)
{
    IBOOL retval = ITRUE;

    bzero(self->buffer, IBUFFER_SIZE);
    self->bufferDataSize = 0;
    return retval;
}

IBOOL InitializeIUDPSocket(IUDPSocket *iUDPSocket, char *addr, int port, ISOCK_DIRECTION sockDirection)
{
    iUDPSocket->port = port;
    iUDPSocket->direction = sockDirection;

    iUDPSocket->oid = IGetObjectId();

    strcpy(iUDPSocket->addrIP, addr);

    iUDPSocket->Create = IUDPSCreate;
    iUDPSocket->Destroy = IUDPSDestroy;
    iUDPSocket->SendTo = IUDPSSendTo;
    iUDPSocket->Recieve = IUDPSRecieve;
    iUDPSocket->ClearData = IUDPSClearData;

    return ITRUE;

}
