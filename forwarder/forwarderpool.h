#ifndef FORWARDERPOOL_H
#define FORWARDERPOOL_H

#include "icommon.h"
#include "itcpforwarder.h"
#include "ithread.h"
#include "iudpforwarder.h"

typedef struct ForwarderPool ForwarderPool;
typedef struct ForwarderPoolObject ForwarderPoolObject;

/**
 * @brief The ForwarderPoolObject object holding connection thread and connection type
 */
struct ForwarderPoolObject {
    ITCPForwarder iTCPForwarder;
    IUDPForwarder iUDPForwarder;
    IThread iThread;
    IPROTO_TYPE proto;
};

/**
 * @brief The ForwarderPool containing ppol objects and limits them to pool size
 */
struct ForwarderPool {
    ForwarderPoolObject pool[IPOOL_SIZE];
    int poolSize;

    IBOOL (*Remove)
    (ForwarderPool* self, int index);
    IBOOL (*RemoveAll)
    (ForwarderPool* self);
};

IBOOL InitializeForwarderPool(ForwarderPool* forwarderPool);
#endif // ifndef FORWARDERPOOL_H
