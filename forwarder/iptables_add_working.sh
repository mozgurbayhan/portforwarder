#!/usr/bin/env bash

#./iptables_flush.sh
#iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 10000:11000 -j DNAT --to 192.168.10.100:10000-11000


#iptables -t nat -A PREROUTING -p tcp --dport 10001 -j DNAT --to-destination 192.168.10.100:10001
#iptables -t nat -A POSTROUTING -p tcp -d 192.168.10.100 --dport 10001 -j SNAT --to-source 192.168.10.101


echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -F
iptables -t nat -F
iptables -X


iptables -t nat -A PREROUTING -p tcp --dport 10000:11000 -j DNAT --to-destination 192.168.10.100:10000-11000
iptables -t nat -A POSTROUTING -p tcp -d 192.168.10.100 --dport 10000:11000 -j SNAT --to-source 192.168.10.101

iptables -S FORWARD
iptables -t nat -vnL PREROUTING
iptables -t nat -vnL POSTROUTING