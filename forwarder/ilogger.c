#include "ilogger.h"
#include "icommon.h"
#include "ithread.h"
#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

char* ep[] = { "ERR", "WRN", "DBG" };
/**
 * @brief ILUdpConnLoop function for logging to UDP
 * @param param
 * @return
 */
int* ILUdpConnLoop(void* param)
{
    SUPPRESS_UNUSED_WARN(param);
    int recvBufSize = 2;
    int recv_len;
    socklen_t slen = sizeof(iLogger.si_other);
    char recvBuf[recvBufSize];

    //    int parami = *( ( int * ) param );

    while (iLogger.isUDPAllowed == ITRUE) {
        // Check New connection
        recv_len = recvfrom(iLogger.sock, recvBuf, recvBufSize, MSG_DONTWAIT, (struct sockaddr*)&(iLogger.si_other), &slen);
        if (recv_len > -1) {
            if (recvBuf[0] == ILOGGER_COMM_REQUEST_STATE)
                iLogger.sessionsRequested = ITRUE;
            iLogger.hasConnection = ITRUE;
        }
        sleep(1);
    }
    return 0;
}

/**
 * @brief UDP Serverstarter function for udp logs
 * @return
 */
IBOOL _ILStartUDPServer()
{
    struct sockaddr_in si_me;
    int tArg = 1;

    if ((iLogger.sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        printf("Servis UDP soketi olusturulamiyor <%s> \n", strerror(errno));
        return IFALSE;
    }

    memset((char*)&si_me, 0, sizeof(si_me));
    memset((char*)&(iLogger.si_other), 0, sizeof(iLogger.si_other));

    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(ILOGGER_PORT);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(iLogger.sock, (struct sockaddr*)&si_me, sizeof(si_me)) == -1) {
        printf("Servis UDP soketi baglanamiyor <%s> \n", strerror(errno));
        return IFALSE;
    }

    InitializeIThread(&(iLogger.serverThread), ILUdpConnLoop);
    iLogger.serverThread.CreateAndRun(&(iLogger.serverThread), &tArg);

    return ITRUE;
}
/**
 * @brief Sends log to udp
 * @param outmsg
 * @return
 */
IBOOL _ILSendToUdp(char* outmsg)
{
    int msgSize;

    if (iLogger.hasConnection) {
        msgSize = strlen(outmsg);
        if (sendto(iLogger.sock, outmsg, msgSize, MSG_DONTWAIT, (struct sockaddr*)&(iLogger.si_other), sizeof(struct sockaddr_in)) == -1)
            iLogger.hasConnection = IFALSE;
    }
    return ITRUE;
}

/**
 * @brief writes log to file
 * @param outmsg
 * @return
 */
IBOOL _ILWriteToFile(char* outmsg)
{
    FILE* f = fopen(ILOGGER_FILE_PATH, "a");
    char timeStr[20];
    time_t now = time(NULL);
    struct tm* time;

    if (f == NULL) {
        printf("Log dosyasina erisilemiyor <%s> \n", strerror(errno));
        return IFALSE;
    } else {
        time = gmtime(&now);
        strftime(timeStr, 20, "%Y-%m-%d_%H:%M:%S", time);
        if (fprintf(f, "%s %s", timeStr, outmsg) < 0)
            printf("Log dosyasina yazilamiyor <%s> \n", strerror(errno));
        if (fclose(f) != 0)
            printf("Log dosyasi kapatilamiyor <%s> \n", strerror(errno));
    }

    return ITRUE;
}
/**
 * @brief public logging function for ILogger
 * @param logType
 * @param msg
 * @return
 */
IBOOL ILLog(ILOG_TYPE logType, char* msg)
{
    int msize = strlen(msg) + 10;
    char* outmsg = (char*)malloc(msize);

    sprintf(outmsg, "%s : %s\n", ep[logType], msg);

    if (iLogger.logToConsole == ITRUE)
        printf("%s", outmsg);
    if (iLogger.logToUdp == ITRUE)
        _ILSendToUdp(outmsg);
    if (iLogger.logToFile == ITRUE)
        _ILWriteToFile(outmsg);

    free(outmsg);
    return ITRUE;
}

IBOOL ILDestroy()
{
    if (iLogger.sock > 0)
        close(iLogger.sock);

    return ITRUE;
}

IBOOL InitializeILogger(IBOOL logToFile, IBOOL logToUdp, IBOOL logToConsole)
{
    IBOOL retval = ITRUE;

    if (iLogger.isEnabled != ITRUE) {
        iLogger.isEnabled = ITRUE;
        iLogger.logToFile = logToFile;
        iLogger.logToUdp = logToUdp;
        iLogger.logToConsole = logToConsole;
        iLogger.isUDPAllowed = ITRUE;
        iLogger.sessionsRequested = IFALSE;
        iLogger.hasConnection = IFALSE;

        iLogger.sock = -1;

        if (logToUdp == ITRUE) {
            retval = _ILStartUDPServer();
            if (retval == IFALSE)
                printf("%s : %s", ep[IERROR], "Logger icin UDP server baslatilamadi\n");
        }

        iLogger.Log = ILLog;
        iLogger.Destroy = ILDestroy;
    } else
        retval = IFALSE;
    return retval;
}
