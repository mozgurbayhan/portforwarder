#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#include "ilogger.h"
#include "itcpsocket.h"
#include "ithread.h"

// //////////////// ITCPSOCKET //////////////////
/**
 * @brief listening thread for ITCPSocket
 * @param itcpsocket
 * @return
 */
int* _ITCPSListenThread(void* itcpsocket)
{
    ITCPSocket* self = (ITCPSocket*)itcpsocket;
    IBOOL tempRet;
    struct sockaddr_in remoteAddr;
    int newConn;
    char msg[256] = { 0 };
    socklen_t csize = sizeof(remoteAddr);

    while (1) {
        if (self->hasPlaceToAccept == ITRUE && self->_hasAcceptedConnection == IFALSE) {
            newConn = accept(self->sock, (struct sockaddr*)&remoteAddr, (socklen_t*)&csize);
            if (newConn == 0) {
                sprintf(msg, "Kabul edilen TCP baglanti numarasi 0 : %s:%d", self->addrIP, self->port);
                iLogger.Log(IERROR, msg);
            } else if (newConn > 0) {
                bzero(&(self->_acceptedConnection), sizeof(ITCPConnection));
                tempRet = InitializeITCPConnection(&(self->_acceptedConnection), self, newConn, &remoteAddr, &self->sockAddrDirEnd);
                if (tempRet == ITRUE)
                    self->_hasAcceptedConnection = ITRUE;
                else {
                    close(newConn);
                    bzero(&(self->_acceptedConnection), sizeof(ITCPConnection));
                }

            } else {
                sprintf(msg, "Kabul edilen TCP baglantisi hatali <%d:%s> at > %s:%d", errno, strerror(errno), self->addrIP, self->port);
                iLogger.Log(IERROR, msg);
            }
        } else
            usleep(500000);
    }
    return 0;
}

int ITCPSCreate(ITCPSocket* self)
{

    char msg[256] = { 0 };

    self->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (self->sock == -1) {
        sprintf(msg, "TCP soketi olusturulamiyor <%d:%s> > %s:%d", errno, strerror(errno), self->addrIP, self->port);
        iLogger.Log(IERROR, msg);
        return IERROR_CANT_CREATE_SOCKET;
    }

    // zero out the structure
    bzero(&(self->sockAddrDirEnd), sizeof(self->sockAddrDirEnd));
    // TODO: adress need to be assigned from param
    self->sockAddrDirEnd.sin_addr.s_addr = INADDR_ANY;
    self->sockAddrDirEnd.sin_family = AF_INET;
    self->sockAddrDirEnd.sin_port = htons(self->port);

    if (self->sockDirection == ISERVER) {
        // TODO: adress need to be assigned from param
        self->sockAddrDirEnd.sin_addr.s_addr = INADDR_ANY;
        // Bind
        if (bind(self->sock, (struct sockaddr*)&(self->sockAddrDirEnd), sizeof(self->sockAddrDirEnd)) < 0) {
            sprintf(msg, "TCP sokete baglanamiyor <%s> > %s:%d", strerror(errno), self->addrIP, self->port);
            iLogger.Log(IERROR, msg);
            return IERROR_CANT_BIND_SOCKET;
        }
        // Listen
        if (listen(self->sock, IMAX_CLIENT_ON_PORT_TCP) < 0) {
            sprintf(msg, "TCP soketi dinleyemiyor <%s> > %s:%d", strerror(errno), self->addrIP, self->port);
            iLogger.Log(IERROR, msg);
            return IERROR_CANT_BIND_SOCKET;
        }

        // Create new thread for listening
        InitializeIThread(&(self->_listenThread), _ITCPSListenThread);
        self->_listenThread.CreateAndRun(&(self->_listenThread), self);
    } else if (self->sockDirection == ICLIENT) {
        self->sockAddrDirEnd.sin_addr.s_addr = inet_addr(self->addrIP);
        if (connect(self->sock, (struct sockaddr*)&(self->sockAddrDirEnd), sizeof(self->sockAddrDirEnd)) < 0)
            return IERROR_CANT_CREATE_SOCKET;
    } else
        return IFALSE;

    return ITRUE;
}

/**
 * @brief ITCPSGetNewConnection triggered when new connection accepted and creates forwarding
 * @param self
 * @param uninitializedConnection
 * @param continueListenAfter
 * @return
 */
IBOOL ITCPSGetNewConnection(ITCPSocket* self, ITCPConnection* uninitializedConnection, IBOOL continueListenAfter)
{
    IBOOL retval = ITRUE;

    if (self->sockDirection == ISERVER) {
        if (self->_hasAcceptedConnection == ITRUE) {
            memcpy(uninitializedConnection, &(self->_acceptedConnection), sizeof(ITCPConnection));
            self->hasPlaceToAccept = continueListenAfter;
            self->_hasAcceptedConnection = IFALSE;
            retval = ITRUE;

        } else
            retval = IFALSE;
    } else if (self->sockDirection == ICLIENT)
        retval = IFALSE;

    else
        retval = IFALSE;
    return retval;
}

/**
 * @brief Destroys the tcp socket
 * @param self
 * @return
 */
IBOOL ITCPSDestroy(ITCPSocket* self)
{
    IBOOL retval = ITRUE;
    char msg[256] = { 0 };

    if (close(self->sock) < 0) {
        sprintf(msg, "TCP soketi kapatilamiyor <%s> > %s:%d", strerror(errno), self->addrIP, self->port);
        iLogger.Log(IERROR, msg);
        retval = IFALSE;
    }
    return retval;
}

IBOOL InitializeITCPSocket(ITCPSocket* iTCPSocket, char* addr, int port, ISOCK_DIRECTION sockDirection)
{
    IBOOL retval = ITRUE;

    iTCPSocket->isAlive = ITRUE;
    iTCPSocket->isInitialized = ITRUE;
    iTCPSocket->oid = IGetObjectId();
    iTCPSocket->_hasAcceptedConnection = IFALSE;
    iTCPSocket->hasPlaceToAccept = ITRUE;

    bzero(iTCPSocket->addrIP, sizeof(iTCPSocket->addrIP));
    strcpy(iTCPSocket->addrIP, addr);
    iTCPSocket->port = port;
    iTCPSocket->sockDirection = sockDirection;

    iTCPSocket->Create = ITCPSCreate;
    iTCPSocket->GetNewConnection = ITCPSGetNewConnection;
    iTCPSocket->Destroy = ITCPSDestroy;

    return retval;
}

// //////////////// ITCPCONNECTION //////////////////

/**
 * @brief Forwards data to target connection
 * @param self
 * @param data
 * @param dataSize
 * @return
 */
IBOOL ITCPCSend(ITCPConnection* self, char* data, int dataSize)
{
    IBOOL retval = ITRUE;
    int tempRet = -1;

    if (self->parentSocket->sockDirection == ISERVER)
        tempRet = send(self->conn, data, dataSize, MSG_DONTWAIT);

    else if (self->parentSocket->sockDirection == ICLIENT)
        tempRet = send(self->parentSocket->sock, data, dataSize, MSG_DONTWAIT);

    else
        return IFALSE;

    if (tempRet > 0) {
        self->lastDataTime = time(NULL);
        retval = ITRUE;
    } else if (tempRet == 0) {
        self->isAlive = IFALSE;
        retval = IFALSE;
    } else
        retval = IFALSE;

    return retval;
}

/**
 * @brief Triggered when new data accepted from listening socket
 * @param self
 * @return
 */
IBOOL ITCPCReceive(ITCPConnection* self)
{
    IBOOL retval = ITRUE;
    int tempRet = -1;

    if (self->parentSocket->sockDirection == ISERVER)
        tempRet = recv(self->conn, self->buffer, IBUFFER_SIZE, MSG_DONTWAIT);

    else if (self->parentSocket->sockDirection == ICLIENT)
        tempRet = recv(self->parentSocket->sock, self->buffer, IBUFFER_SIZE, MSG_DONTWAIT);
    else
        return IFALSE;

    if (tempRet > 0) {
        self->bufferDataSize = tempRet;
        self->lastDataTime = time(NULL);
    } else if (tempRet == 0) {
        self->isAlive = IFALSE;
        retval = IFALSE;
    } else
        retval = IFALSE;

    return retval;
}

/**
 * @brief Clears transferred data
 * @param self
 * @return
 */
IBOOL ITCPCClearData(ITCPConnection* self)
{
    IBOOL retval = ITRUE;

    bzero(self->buffer, IBUFFER_SIZE);
    self->bufferDataSize = 0;
    return retval;
}

/**
 * @brief ITCPCClose closes connections
 * @param self
 * @return
 */
IBOOL ITCPCClose(ITCPConnection* self)
{
    IBOOL retval = ITRUE;
    char msg[256] = { 0 };

    self->ClearData(self);

    // Eger connection kapatilirsa son connection file descriptor'i kapatiyor
    // dolayisiyla sonraki conn id 0 geliyor
    // Bu da hata olusturuyor
    // Asagidaki koddan sakinmali!!!

    //    if (close(self->conn) < 0)
    //        retval = IFALSE;

    if (self->parentSocket->sockDirection == ISERVER)
        if (close(self->conn) < 0) {

            sprintf(msg, "Baglanti kapatilamiyor <%s> >,Dinleyici %s:%d", strerror(errno), self->parentSocket->addrIP, self->parentSocket->port);
            iLogger.Log(IERROR, msg);

            retval = IFALSE;
        }

    return retval;
}

IBOOL InitializeITCPConnection(ITCPConnection* iTCPConnection, ITCPSocket* iTCPSocket, int conn, struct sockaddr_in* sockAddrFrom, struct sockaddr_in* sockAddrTo)
{
    iTCPConnection->isInitialized = ITRUE;
    iTCPConnection->isAlive = ITRUE;
    iTCPConnection->parentSocket = iTCPSocket;
    iTCPConnection->conn = conn;
    iTCPConnection->oid = IGetObjectId();

    memcpy(&(iTCPConnection->sockAddrFrom), sockAddrFrom, sizeof(struct sockaddr_in));
    memcpy(&(iTCPConnection->sockAddrTo), sockAddrTo, sizeof(struct sockaddr_in));

    iTCPConnection->Send = ITCPCSend;
    iTCPConnection->Receive = ITCPCReceive;
    iTCPConnection->ClearData = ITCPCClearData;
    iTCPConnection->Close = ITCPCClose;
    iTCPConnection->lastDataTime = time(NULL);
    return ITRUE;
}
