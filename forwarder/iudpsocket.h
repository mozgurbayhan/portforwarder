#ifndef IUDPSOCKET_H
#define IUDPSOCKET_H

#include <netinet/in.h>
#include "icommon.h"


typedef struct IUDPSocket IUDPSocket;

/**
 * @brief The IUDPSocket object holds server or client socket info
 */
struct IUDPSocket
{

    char addrIP[IMAX_IP_STRING_LENGTH];
    int port;
    int sock;
    unsigned long  oid;
    ISOCK_DIRECTION direction;

    char buffer[IBUFFER_SIZE];
    int bufferDataSize;

    time_t lastDataTime;

    struct sockaddr_in sockAddrLocal;
    struct sockaddr_in sockAddrRemote;

    int (*Create)(IUDPSocket *self);
    IBOOL (*Destroy)(IUDPSocket *self);
    IBOOL (*SendTo)(IUDPSocket *self, char *data, int dataSize);
    IBOOL (*Recieve)(IUDPSocket *self);
    IBOOL (*ClearData)(IUDPSocket *self);


//    int (*initializeClient)(ISocket *self);
};

IBOOL InitializeIUDPSocket(IUDPSocket *iUDPSocket, char *addr, int port, ISOCK_DIRECTION sockDirection);
#endif // ifndef ISOCKET_H
