# -*- coding: utf-8 -*-
__author__ = 'ozgur'
__creation_date__ = '27.02.2017' '15:24'

PREFIX = """
#!/bin/sh
echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -F
iptables -t nat -F
iptables -X

"""

# SUFFIX = """
#
# iptables -S FORWARD
# iptables -t nat -vnL PREROUTING
# iptables -t nat -vnL POSTROUTING
# """

SUFFIX = ""

RULE = """
iptables -t nat -A PREROUTING -p {PROTO} --dport {PORTSERV} -j DNAT --to-destination {IPDEST}:{PORTDEST}
iptables -t nat -A POSTROUTING -p {PROTO} -d {IPDEST} --dport {PORTDEST} -j SNAT --to-source {IPSERV}
"""

V_PORTSERV = "{PORTSERV}"
V_PORTDEST = "{PORTDEST}"
V_IPDEST = "{IPDEST}"
V_IPSERV = "{IPSERV}"
V_PROTO = "{PROTO}"


class Protocol:
    TCP = "tcp"
    UDP = "udp"

    def __init__(self):
        pass


def create_random_for_tcp():
    chain = ""
    for x in range(2):
        pserv = str(10000 + x)
        pdest = str(10000 + x)
        ipdest = "192.168.10.100"
        ipserv = "192.168.10.101"
        rule = RULE.replace(V_PORTSERV, pserv).replace(V_PORTDEST, pdest).replace(V_IPDEST, ipdest).replace(V_IPSERV, ipserv).replace(V_PROTO, Protocol.TCP)
        chain += rule

    data = PREFIX + chain + SUFFIX
    f = open("iptables_add.sh", "w")
    f.write(data)
    f.close()


def create_random_for_udp():
    chain = ""
    for x in range(200):
        pserv = str(10000 + x)
        pdest = str(10000)
        ipserv = "192.168.10.101"
        if x + 2 == 101:
            ipdest = "192.168.10.250"
        else:
            ipdest = "192.168.10.%d" % (x + 2,)
        rule = RULE.replace(V_PORTSERV, pserv).replace(V_PORTDEST, pdest).replace(V_IPDEST, ipdest).replace(V_IPSERV, ipserv).replace(V_PROTO, Protocol.UDP)
        chain += rule

    data = PREFIX + chain + SUFFIX
    f = open("iptables_add.sh", "w")
    f.write(data)
    f.close()


if __name__ == '__main__':
    create_random_for_udp()
    # create_random_for_tcp()

    #
