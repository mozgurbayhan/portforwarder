#ifndef ICONFIG_H
#define ICONFIG_H

#include "icommon.h"

typedef struct IConfigItem IConfigItem;
typedef struct IConfig IConfig;

/**
 * @brief The IConfigItem object for every line of config file
 */
struct IConfigItem {
    IPROTO_TYPE proto;
    char *fromAddr, *toAddr;
    int fromPort, toPort;
};

/**
 * @brief The IConfig object refers to whole config file
 */
struct IConfig {
    char* filepath;
    IConfigItem configItems[IPOOL_SIZE];
    int configItemCount;

    int (*ReadConfig)(IConfig* self, char* filepath);
};

IBOOL InitializeIConfig(IConfig* iConfig);
#endif // ifndef ICONFIG_H
