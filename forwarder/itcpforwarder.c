#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "icommon.h"
#include "ilogger.h"
#include "itcpforwarder.h"

/**
 * @brief _ITCPFAddTCPIForwardObject adds accepted ITCPConnection object defined in itcpsocket.h
 * @param self
 * @param acceptedConnection
 * @return
 */
IBOOL _ITCPFAddTCPIForwardObject(ITCPForwarder* self, ITCPConnection* acceptedConnection)
{

    ITCPSocket* clientSocket = &(self->sessions[self->sessionsCount].requestedSocket);
    ITCPConnection* clientConnection = &(self->sessions[self->sessionsCount].requestedConn);
    int tempRetI;

    InitializeITCPSocket(clientSocket, self->targetAddr, self->targetPort, ICLIENT);

    tempRetI = clientSocket->Create(clientSocket);
    if (tempRetI == ITRUE)
        InitializeITCPConnection(clientConnection, clientSocket, IFALSE, &(acceptedConnection->sockAddrFrom), &(acceptedConnection->sockAddrTo));
    else {
        acceptedConnection->Close(acceptedConnection);
        return IFALSE;
    }
    self->sessions[self->sessionsCount].oid = IGetObjectId();
    memcpy(&(self->sessions[self->sessionsCount].acceptedConn), acceptedConnection, sizeof(ITCPConnection));

    self->sessionsCount++;

    return ITRUE;
}
/**
 * @brief _ITCPFDeleteIForwardObject deletes accepted ITCPConnection object defined in itcpsocket.h
 * @param self
 * @param index
 * @return
 */
IBOOL _ITCPFDeleteIForwardObject(ITCPForwarder* self, int index)
{
    ITCPSession dumbSession;

    bzero(&dumbSession, sizeof(ITCPSession));
    int cursor;

    self->sessions[index].acceptedConn.Close(&(self->sessions[index].acceptedConn));
    self->sessions[index].requestedConn.Close(&(self->sessions[index].requestedConn));
    self->sessions[index].requestedSocket.Destroy(&(self->sessions[index].requestedSocket));
    self->sessions[index] = dumbSession;
    if (index + 1 < self->sessionsCount) {
        for (cursor = index; cursor < self->sessionsCount - 1; cursor++) {
            self->sessions[cursor] = self->sessions[cursor + 1];
            self->sessions[cursor + 1] = dumbSession;
            // Fix Pointers
            self->sessions[cursor].requestedConn.parentSocket = &(self->sessions[cursor].requestedSocket);
        }
    }
    self->sessionsCount--;
    return ITRUE;
}

int ITCPFCreate(ITCPForwarder* self)
{

    return self->listenSocket.Create(&(self->listenSocket));
}

/**
 * @brief ITCPFForward forwards incoming data to target connection
 * @param self
 * @return
 */
IBOOL ITCPFForward(ITCPForwarder* self)
{
    IBOOL tempRetB;
    IBOOL continueListenAfter;
    ITCPConnection tempConnection;
    ITCPSession* iso;
    int i;
    time_t now;
    char msg[256] = { 0 };
    char tmpMsg[256] = { 0 };

    // Check new connections
    if (self->sessionsCount < IMAX_CLIENT_ON_PORT_TCP) {

        if (self->sessionsCount + 1 == IMAX_CLIENT_ON_PORT_TCP)
            continueListenAfter = IFALSE;
        else
            continueListenAfter = ITRUE;

        tempRetB = self->listenSocket.GetNewConnection(&(self->listenSocket), &tempConnection, continueListenAfter);
        if (tempRetB == ITRUE) {
            bzero(msg, 256);
            bzero(tmpMsg, 256);
            sprintf(tmpMsg, "ADD TCP -> L: %s:%d | R: %s:%d ,ICONNID:%lu -> ", self->listenAddr, self->listenPort,
                inet_ntoa(tempConnection.sockAddrFrom.sin_addr), (int)ntohs(tempConnection.sockAddrFrom.sin_port),
                tempConnection.oid);
            tempRetB = _ITCPFAddTCPIForwardObject(self, &tempConnection);
            if (tempRetB == ITRUE)
                sprintf(msg, "%s Done!", tmpMsg);
            else
                sprintf(msg, "%s Fail!", tmpMsg);
            iLogger.Log(IDEBUG, msg);
        }
    }

    // Transfer data
    for (i = 0; i < self->sessionsCount; i++) {
        iso = &(self->sessions[i]);

        if (iso->acceptedConn.Receive(&(iso->acceptedConn)) == ITRUE) {
            iso->requestedConn.Send(&(iso->requestedConn), iso->acceptedConn.buffer, iso->acceptedConn.bufferDataSize);
            iso->acceptedConn.ClearData(&(iso->acceptedConn));
        }

        if (iso->requestedConn.Receive(&(iso->requestedConn)) == ITRUE) {
            iso->acceptedConn.Send(&(iso->acceptedConn), iso->requestedConn.buffer, iso->requestedConn.bufferDataSize);
            iso->requestedConn.ClearData(&(iso->requestedConn));
        }
    }

    // Cleanup
    time(&now);
    for (i = self->sessionsCount - 1; i > -1; i--) {
        iso = &(self->sessions[i]);

        if (iso->requestedConn.isAlive == IFALSE || iso->acceptedConn.isAlive == IFALSE || (difftime(now, iso->acceptedConn.lastDataTime) > IMAX_SOCKET_IDLE_TIME_TCP || difftime(now, iso->acceptedConn.lastDataTime) > IMAX_SOCKET_IDLE_TIME_TCP)) {
            sprintf(msg, "DEL TCP -> L: %s:%d | R: %s:%d ,ICONNID:%lu", self->listenAddr, self->listenPort,
                inet_ntoa(iso->acceptedConn.sockAddrFrom.sin_addr), (int)ntohs(iso->acceptedConn.sockAddrFrom.sin_port),
                iso->acceptedConn.oid);
            iLogger.Log(IDEBUG, msg);
            _ITCPFDeleteIForwardObject(self, i);
            self->listenSocket.hasPlaceToAccept = ITRUE;
        }
    }

    return ITRUE;
}
IBOOL ITCPFDestroy(ITCPForwarder* self)
{
    int i;
    IBOOL tmpVal;

    for (i = self->sessionsCount - 1; i > -1; i--) {
        tmpVal = _ITCPFDeleteIForwardObject(self, i);
        if (tmpVal == IFALSE)
            return IFALSE;
    }

    free(self->listenAddr);
    free(self->targetAddr);

    return ITRUE;
}

IBOOL InitializeITCPForwarder(ITCPForwarder* iTCPForwarder, char* listenAddr, char* targetAddr, int listenPort, int targetPort)
{

    IBOOL retval = ITRUE;

    InitializeITCPSocket(&(iTCPForwarder->listenSocket), listenAddr, listenPort, ISERVER);

    iTCPForwarder->sessionsCount = 0;
    iTCPForwarder->isEnabled = ITRUE;
    iTCPForwarder->oid = IGetObjectId();

    iTCPForwarder->listenAddr = malloc(strlen(listenAddr) + 1);
    iTCPForwarder->targetAddr = malloc(strlen(targetAddr) + 1);
    strcpy(iTCPForwarder->listenAddr, listenAddr);
    strcpy(iTCPForwarder->targetAddr, targetAddr);
    iTCPForwarder->listenPort = listenPort;
    iTCPForwarder->targetPort = targetPort;

    iTCPForwarder->Create = ITCPFCreate;
    iTCPForwarder->Forward = ITCPFForward;
    iTCPForwarder->Destroy = ITCPFDestroy;

    return retval;
}
