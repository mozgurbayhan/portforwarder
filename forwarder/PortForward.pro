TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    ithread.c \
    forwarderpool.c \
    main.c \
    icommon.c \
    itcpsocket.c \
    itcpforwarder.c \
    iudpsocket.c \
    iudpforwarder.c \
    iconfig.c \
    ilogger.c
LIBS += -pthread

HEADERS += \
    icommon.h \
    ithread.h \
    forwarderpool.h \
    itcpsocket.h \
    itcpforwarder.h \
    iudpsocket.h \
    iudpforwarder.h \
    iconfig.h \
    ilogger.h

DISTFILES += \
    rules.conf \
    buildme.sh \
    monitor.sh \
    rules_stress.conf
