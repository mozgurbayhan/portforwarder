#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#include "icommon.h"
#include "iforwarder.h"
#include "itcpconnection.h"


IBOOL _IFAddTCPIForwardObject(IForwarder *self, ITCPConnection *acceptedConnection)
{

    ITCPSocket *clientSocket = &(self->sessions[self->sessionsCount].requestedSocket);
    ITCPConnection *clientConnection = &(self->sessions[self->sessionsCount].requestedConn);
    int tempRetI;

    // TODO: Here is the problem
    InitializeISocket(clientSocket, self->targetAddr, self->targetPort, ICLIENT);

    tempRetI = clientSocket->Create(clientSocket);
    if (tempRetI == ITRUE)
        InitializeIConnection(clientConnection, clientSocket, IFALSE);
    else
    {
        acceptedConnection->Close(acceptedConnection);
        return IFALSE;
    }
    self->sessions[self->sessionsCount].oid = IGetObjectId();
    memcpy(&(self->sessions[self->sessionsCount].acceptedConn), acceptedConnection, sizeof(ITCPConnection));


    self->sessionsCount++;

    return ITRUE;

}

IBOOL _IFDeleteIForwardObject(IForwarder *self, int index)
{
    ISession dumbSession;
    int cursor;

    self->sessions[index].acceptedConn.Close(&(self->sessions[index].acceptedConn));
    self->sessions[index].requestedConn.Close(&(self->sessions[index].requestedConn));
    self->sessions[index].requestedSocket.Destroy(&(self->sessions[index].requestedSocket));
    self->sessions[index] = dumbSession;
    if (index + 1 < self->sessionsCount)
    {
        for (cursor = index; cursor < self->sessionsCount - 1; cursor++ )
        {
            self->sessions[cursor] = self->sessions[cursor + 1];
            self->sessions[cursor + 1] = dumbSession;
        }
    }
    self->sessionsCount--;
    return ITRUE;
}



int IFCreate(IForwarder *self)
{

    return self->listenSocket.Create(&(self->listenSocket));

}


IBOOL IFForward(IForwarder *self)
{
    IBOOL tempRetB;
    ITCPConnection tempConnection;
    ISession *iso;
    int i;
    time_t now;

    // Check new connections
    if (self->sessionsCount < IMAX_CLIENT_ON_PORT)
    {
        tempRetB = self->listenSocket.NewConnection(&(self->listenSocket), &tempConnection);
        if (tempRetB == ITRUE)
            _IFAddTCPIForwardObject(self, &tempConnection);
//            printf("Got Connection -> %s:%d > %s:%d -> count:%d\n", self->listenAddr, self->listenPort, self->targetAddr, self->targetPort,self->forwardObjectsCount);

    }

    // Transfer data
    for (i = 0; i < self->sessionsCount; i++)
    {
        iso = &(self->sessions[i]);

        if (iso->acceptedConn.Receive(&(iso->acceptedConn)) == ITRUE)
        {
            iso->requestedConn.Send(&(iso->requestedConn), iso->acceptedConn.buffer, iso->acceptedConn.bufferDataSize);
            iso->acceptedConn.ClearData(&(iso->acceptedConn));
        }

        if (iso->requestedConn.Receive(&(iso->requestedConn)) == ITRUE)
        {
            iso->acceptedConn.Send(&(iso->acceptedConn), iso->requestedConn.buffer, iso->requestedConn.bufferDataSize);
            iso->requestedConn.ClearData(&(iso->requestedConn));
        }
    }



    // Cleanup
    time(&now);
    for (i = self->sessionsCount - 1; i > -1; i--)
    {
        iso = &(self->sessions[i]);

        if (iso->requestedConn.isAlive == IFALSE ||
            iso->acceptedConn.isAlive == IFALSE ||
            (
                difftime(now, iso->requestedConn.lastDataTime) > IMAX_SOCKET_IDLE_TIME_TCP &&
                difftime(now, iso->acceptedConn.lastDataTime) > IMAX_SOCKET_IDLE_TIME_TCP)
            )
        {
            _IFDeleteIForwardObject(self, i);
            printf("Got Connection -> %s:%d > %s:%d -> obj:%d/%d\n", self->listenAddr, self->listenPort, self->targetAddr, self->targetPort, i, self->sessionsCount);
        }
    }
    return ITRUE;

}

IBOOL IFDestroy(IForwarder *self)
{
    int i;
    IBOOL tmpVal;

    for (i = self->sessionsCount - 1; i > -1; i--)
    {
        tmpVal = _IFDeleteIForwardObject(self, i);
        if (tmpVal == IFALSE)
            return IFALSE;
    }

    free(self->listenAddr);
    free(self->targetAddr);

    return ITRUE;
}

IBOOL InitializeIForwarder(IForwarder *iForwarder, char *listenAddr, char *targetAddr, int listenPort, int targetPort, ICONN_TYPE connType)
{

    IBOOL retval = ITRUE;

    InitializeISocket(&(iForwarder->listenSocket), listenAddr, listenPort,  ISERVER);
//    if (ICONN_TYPE == UDP)
//        InitializeIConnection(&(iForwarder->listenConnUDP), &(iForwarder->listenSocket), IFALSE, IFALSE);

    iForwarder->sessionsCount = 0;
    iForwarder->isEnabled = ITRUE;
    iForwarder->oid = IGetObjectId();

    iForwarder->listenAddr = malloc(sizeof(listenAddr));
    iForwarder->targetAddr = malloc(sizeof(targetAddr));
    strcpy(iForwarder->listenAddr, listenAddr);
    strcpy(iForwarder->targetAddr, targetAddr);
    iForwarder->listenPort = listenPort;
    iForwarder->targetPort = targetPort;

    iForwarder->Create = IFCreate;
    iForwarder->Forward = IFForward;
    iForwarder->Destroy = IFDestroy;

    return retval;
}
