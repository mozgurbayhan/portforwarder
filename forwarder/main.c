#include "forwarderpool.h"
#include "icommon.h"
#include "iconfig.h"
#include "ilogger.h"
#include "itcpforwarder.h"
#include "itcpsocket.h"
#include "ithread.h"
#include "iudpforwarder.h"
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

// #include "otcpserver.h"
// #include "otcpclient.h"
// #include "oserverpool.h"

volatile sig_atomic_t keepRunning = 1;

ForwarderPool fp;

/**
 * @brief intHandler interrupt handler which is responsible for garbage collecting
 * and terminating the app for a clean exit
 * @param exitCode
 */
void intHandler(int exitCode)
{
    SUPPRESS_UNUSED_WARN(exitCode);
    //    char ecs[20];
    //    iLogger.Log(IDEBUG, "TERMINATE SIGNAL CATCHED");
    //    sprintf(ecs, "Exit code:%d\n", exitCode);
    //    iLogger.Log(IDEBUG, ecs);
    fp.RemoveAll(&fp);
    iLogger.Destroy();

    keepRunning = 0;
}

/**
 * @brief TCPUDPThreadFunction configures a ForwarderPoolObject for TCP and UDP
 * @param iForwarderPoolObject
 * @return
 */
int* TCPUDPThreadFunction(void* iForwarderPoolObject)
{
    ForwarderPoolObject* po = (ForwarderPoolObject*)iForwarderPoolObject;

    while (po->iThread.isAllowed) {
        if (po->iTCPForwarder.isEnabled == ITRUE)
            po->iTCPForwarder.Forward(&(po->iTCPForwarder));
        if (po->iUDPForwarder.isEnabled == ITRUE)
            po->iUDPForwarder.Forward(&(po->iUDPForwarder));
        usleep(ILOOP_DELAY);
    }
    return 0;
}

/**
 * @brief TCPThreadFunction configures a ForwarderPoolObject for TCP
 * @param iForwarderPoolObject
 * @return
 */
int* TCPThreadFunction(void* iForwarderPoolObject)
{
    ForwarderPoolObject* po = (ForwarderPoolObject*)iForwarderPoolObject;

    while (po->iThread.isAllowed) {
        if (po->iTCPForwarder.isEnabled == ITRUE)
            po->iTCPForwarder.Forward(&(po->iTCPForwarder));
        usleep(ILOOP_DELAY);
    }
    return 0;
}

/**
 * @brief UDPThreadFunction configures a ForwarderPoolObject for UDP
 * @param iForwarderPoolObject
 * @return
 */
int* UDPThreadFunction(void* iForwarderPoolObject)
{
    ForwarderPoolObject* po = (ForwarderPoolObject*)iForwarderPoolObject;

    while (po->iThread.isAllowed) {
        if (po->iUDPForwarder.isEnabled == ITRUE)
            po->iUDPForwarder.Forward(&(po->iUDPForwarder));
        usleep(ILOOP_DELAY);
    }
    return 0;
}

/**
 * @brief printState prints pools state to the allowed loggers
 * @param fp
 */
void printState(ForwarderPool* fp)
{
    int i;
    char msg[256] = { 0 };
    IBOOL hasConnection = IFALSE;

    for (i = 0; i < fp->poolSize; i++) {
        bzero(msg, 256);
        if (fp->pool[i].proto == IUDP && fp->pool[i].iUDPForwarder.sessionsCount > 0) {
            sprintf(msg, "SC: %d >> UDP >> %s:%d > %s:%d",
                fp->pool[i].iUDPForwarder.sessionsCount,
                fp->pool[i].iUDPForwarder.listenAddr,
                fp->pool[i].iUDPForwarder.listenPort,
                fp->pool[i].iUDPForwarder.targetAddr,
                fp->pool[i].iUDPForwarder.targetPort);
            iLogger.Log(IDEBUG, msg);
            hasConnection = ITRUE;
        } else if (fp->pool[i].proto == ITCP && fp->pool[i].iTCPForwarder.sessionsCount > 0) {
            sprintf(msg, "SC: %d >> TCP >> %s:%d > %s:%d",
                fp->pool[i].iTCPForwarder.sessionsCount,
                fp->pool[i].iTCPForwarder.listenAddr,
                fp->pool[i].iTCPForwarder.listenPort,
                fp->pool[i].iTCPForwarder.targetAddr,
                fp->pool[i].iTCPForwarder.targetPort);
            iLogger.Log(IDEBUG, msg);
            hasConnection = ITRUE;
        } else if (fp->pool[i].proto == ITCPUDP && fp->pool[i].iUDPForwarder.sessionsCount > 0) {

            sprintf(msg, "SC: %d >> UDP* >> %s:%d > %s:%d",
                fp->pool[i].iUDPForwarder.sessionsCount,
                fp->pool[i].iUDPForwarder.listenAddr,
                fp->pool[i].iUDPForwarder.listenPort,
                fp->pool[i].iUDPForwarder.targetAddr,
                fp->pool[i].iUDPForwarder.targetPort);
            iLogger.Log(IDEBUG, msg);
            hasConnection = ITRUE;
        } else if (fp->pool[i].proto == ITCPUDP && fp->pool[i].iTCPForwarder.sessionsCount > 0) {

            sprintf(msg, "SC: %d >> TCP* >> %s:%d > %s:%d",
                fp->pool[i].iTCPForwarder.sessionsCount,
                fp->pool[i].iTCPForwarder.listenAddr,
                fp->pool[i].iTCPForwarder.listenPort,
                fp->pool[i].iTCPForwarder.targetAddr,
                fp->pool[i].iTCPForwarder.targetPort);
            iLogger.Log(IDEBUG, msg);
            hasConnection = ITRUE;
        }
    }
    if (hasConnection == IFALSE)
        iLogger.Log(IDEBUG, "SC: Yok!");
}

/**
 * @brief RunMe is main run function
 * @return
 */
int RunMe()
{
    IConfig ic;
    ForwarderPoolObject* tmpPo;
    IConfigItem* ici;

    IThread* tmpThread;
    ITCPForwarder* tmpTCPForwarder;
    IUDPForwarder* tmpUDPForwarder;
    int i;
    int tempRetI = IFALSE;

    char configPath[] = "rules.conf";
    char msg[256] = { 0 };

    InitializeIConfig(&ic);
    ic.ReadConfig(&ic, configPath);

    InitializeForwarderPool(&fp);

    for (i = 0; i < ic.configItemCount; i++) {
        ici = &(ic.configItems[i]);
        tmpPo = &(fp.pool[i]);
        tmpTCPForwarder = &(tmpPo->iTCPForwarder);
        tmpUDPForwarder = &(tmpPo->iUDPForwarder);
        tmpThread = &(tmpPo->iThread);

        usleep(1000); //To ensure thread creation

        if (ic.configItems[i].proto == ITCP) {
            tmpPo->proto = ITCP;
            // initialize tcp
            InitializeITCPForwarder(tmpTCPForwarder, ici->fromAddr, ici->toAddr, ici->fromPort, ici->toPort);
            tempRetI = tmpTCPForwarder->Create(tmpTCPForwarder);
            if (tempRetI != ITRUE) {
                sprintf(msg, "TCP dinleyiciyi olustururken hata = %d ~<%s> -> %s:%d! -> Cikiliyor...\n", tempRetI, strerror(errno), ici->fromAddr, ici->fromPort);
                iLogger.Log(IERROR, msg);
                exit(-1);
            }
            // initialize thread
            InitializeIThread(tmpThread, TCPThreadFunction);
        } else if (ic.configItems[i].proto == IUDP) {

            tmpPo->proto = IUDP;
            // initialize udp
            InitializeIUDPForwarder(tmpUDPForwarder, ici->fromAddr, ici->toAddr, ici->fromPort, ici->toPort);
            tempRetI = tmpUDPForwarder->Create(tmpUDPForwarder);
            if (tempRetI != ITRUE) {
                sprintf(msg, "UDP dinleyiciyi olustururken hata = %d -> %s:%d! -> Cikiliyor...\n", tempRetI, ici->fromAddr, ici->fromPort);
                iLogger.Log(IERROR, msg);
                exit(-1);
            }
            // initialize thread
            InitializeIThread(tmpThread, UDPThreadFunction);
        } else if (ic.configItems[i].proto == ITCPUDP) {

            tmpPo->proto = ITCPUDP;

            // initialize tcp
            InitializeITCPForwarder(tmpTCPForwarder, ici->fromAddr, ici->toAddr, ici->fromPort, ici->toPort);
            tmpTCPForwarder->Create(tmpTCPForwarder);
            // initialize udp
            InitializeIUDPForwarder(tmpUDPForwarder, ici->fromAddr, ici->toAddr, ici->fromPort, ici->toPort);
            tmpUDPForwarder->Create(tmpUDPForwarder);
            // initialize thread
            InitializeIThread(tmpThread, TCPUDPThreadFunction);
        } else {
            printf("Fatal error in parsed config file !\n");
            exit(0);
        }

        tmpThread->CreateAndRun(tmpThread, tmpPo);
        fp.poolSize++;
    }
    while (keepRunning == 1) {
        sleep(1);
        if (iLogger.sessionsRequested == ITRUE) {
            printState(&fp);
            iLogger.sessionsRequested = IFALSE;
        }
    }

    return 0;
}

int main(int argc, char* argv[])
{
    IBOOL logToFile = IFALSE;
    IBOOL logToUdp = IFALSE;
    IBOOL logToConsole = IFALSE;
    int i;
    int retval;

    if (argc > 1) {
        if (strcmp(argv[1], "-h") == 0) {
            printf("PortForwarder.bin [-h, -v, -m, -l]\n");
            printf("  Options:\n");
            printf("  -h: prints this help. \n");
            printf("  -v: log to console. \n");
            printf("  -m: log to UDP server. \n");
            printf("  -l: log to file. \n");
            exit(0);
        } else {
            for (i = 1; i < argc; i++) {
                if (strcmp(argv[i], "-v") == 0)
                    logToConsole = ITRUE;
                else if (strcmp(argv[i], "-m") == 0)
                    logToUdp = ITRUE;
                else if (strcmp(argv[i], "-l") == 0)
                    logToFile = ITRUE;
            }
        }
        //        else if (strcmp(argv[1], "-m") == 0)
        //            monitor = ITRUE;
    }

    if (InitializeILogger(logToFile, logToUdp, logToConsole) == IFALSE)
        exit(0);
    signal(SIGINT, intHandler);
    iLogger.Log(IDEBUG, "PORT FORWARDER STARTED");
    retval = RunMe();
    iLogger.Log(IDEBUG, "PORT FORWARDER STOPPED");
    return retval;
}

// Thread sayisini arttirabilmek icin stack size 4092'ye indirilmeli
// ulimit -s 4092
// Kontrol edilen baglanti sayisini arttırmak icin open file sayisi arttirilmali.
// ulimit -n 2048
