#include "icommon.h"

unsigned long _ILastObjectId = 0;

/**
 * @brief helper function returning object id of object-like structures
 * @return
 */
unsigned long IGetObjectId()
{
    _ILastObjectId++;
    return _ILastObjectId - 1;
}
