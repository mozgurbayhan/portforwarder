#!/bin/bash

# -g :enable debug symbols

gcc \
-Wall \
-pthread \
icommon.h \
ilogger.h \
ithread.h \
forwarderpool.h \
itcpsocket.h \
itcpforwarder.h \
iudpsocket.h \
iudpforwarder.h \
iconfig.h \
ithread.c \
forwarderpool.c \
main.c \
icommon.c \
ilogger.c \
itcpsocket.c \
itcpforwarder.c \
iudpsocket.c \
iudpforwarder.c \
iconfig.c \
-o PortForwarder.bin

