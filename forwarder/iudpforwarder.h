#ifndef IUDPFORWARDER_H
#define IUDPFORWARDER_H

#include "icommon.h"
#include "iudpsocket.h"

typedef struct IUDPForwarder IUDPForwarder;
typedef struct IUDPSession IUDPSession;

/**
 * @brief The IUDPSession holds accepted and requested connection info
 */
struct IUDPSession {
    struct sockaddr_in acceptedAddress;
    time_t lastAcceptedDataTime;
    IUDPSocket requestedSocket;
    unsigned long oid;
};

/**
 * @brief The IUDPForwarder object is the main listener and forwarder described in config file
 */
struct IUDPForwarder {
    IUDPSocket listenSocket;

    IUDPSession sessions[IMAX_CLIENT_ON_PORT_UDP];
    int sessionsCount;
    unsigned long oid;
    int listenPort, targetPort;
    char* listenAddr;
    char* targetAddr;
    IBOOL isEnabled;

    int (*Create)(IUDPForwarder* self);
    IBOOL (*Forward)
    (IUDPForwarder* self);
    IBOOL (*Destroy)
    (IUDPForwarder* self);
};

IBOOL InitializeIUDPForwarder(IUDPForwarder* iUDPForwarder, char* listenAddr, char* targetAddr, int listenPort, int targetPort);
#endif // ifndef IUDPFORWARDER_H
