# Port Forwarder

## Description 

PF is an fully working application level port forwarding application for linux based systems. Replacement of iptables. You can easily set the rules by using config file.

For example:

	TCPUDP|192.168.0.36|10000|192.168.1.142|3000

listens port 10000 of 192.168.0.36 address for TCP and UDP connections and routes packages to port 3000 of 192.168.1.142.

Every connection creates a new posix thread. CPU and RAM usage results are under documentation folder compared to iptables. Unfortunately it is only in Turkish.

The main code is written by using C , stress and blackbox test tools are wriiten by using python and pyqt. Also some bash script used for result comparasion.


**C is a structural language but i converted it partially object oriented approach. Static classes and object instances are implemented by helping of struct and function mappings.**

Flowchart and design diagrams can be found in "**[documentation/DesignDiagrams/](https://gitlab.com/mozgurbayhan/portforwarder/tree/master/documentation/DesignDiagrams)**" .


***


## Code Structure

Project is coded in QtCreator. So feel free to use project file in source code for an easy implementation.

***

### Basic Components:

**main.c:** The main code runs in function RunMe()

**icommon.c/h :** Contains basic configuration, custom exception definations and custom types like boolean equvalent IBOOL. Also IGetObjectId() function defined in there which is the backbone part of OOP approach in C.

**ilogger.c :** 3 types of logging allowed. To console, to file or to an UDP server.

**iconfig.c :** Responsible of reading rules from config file and create config objects per line.

Diagram is in here: 
![](https://gitlab.com/mozgurbayhan/portforwarder/raw/master/documentation/DesignDiagrams/ClassDiagram.jpeg) 

***

### Basic Flow:

1. When started, servers are created in a posix thread as described in config file.
2. When a new connection request came, it checks the connection pool and establishes a new connection if pool has space.
3. Every connection also triggers creation of a a new connection to the target as described in config file.
4. Data is transferred until incoming or outgoing connection ends by remotely.
5. After connection lost garbage collectors cleans the objects and frees a space in pool.
6. Returns 2

Diagram is in here: 
![](https://gitlab.com/mozgurbayhan/portforwarder/raw/master/documentation/DesignDiagrams/FlowDiagram.jpeg) 