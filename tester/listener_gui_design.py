# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'listener_gui_design.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1267, 881)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("resources/Download.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab_3 = QtGui.QWidget()
        self.tab_3.setObjectName(_fromUtf8("tab_3"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.tab_3)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label = QtGui.QLabel(self.tab_3)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_2.addWidget(self.label)
        self.txtPort = QtGui.QLineEdit(self.tab_3)
        self.txtPort.setMaximumSize(QtCore.QSize(70, 16777215))
        self.txtPort.setInputMethodHints(QtCore.Qt.ImhNone)
        self.txtPort.setObjectName(_fromUtf8("txtPort"))
        self.horizontalLayout_2.addWidget(self.txtPort)
        self.btnCreate = QtGui.QPushButton(self.tab_3)
        self.btnCreate.setObjectName(_fromUtf8("btnCreate"))
        self.horizontalLayout_2.addWidget(self.btnCreate)
        self.label_2 = QtGui.QLabel(self.tab_3)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_2.addWidget(self.label_2)
        self.txtTotal = QtGui.QLineEdit(self.tab_3)
        self.txtTotal.setMaximumSize(QtCore.QSize(100, 16777215))
        self.txtTotal.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.txtTotal.setReadOnly(True)
        self.txtTotal.setObjectName(_fromUtf8("txtTotal"))
        self.horizontalLayout_2.addWidget(self.txtTotal)
        self.label_3 = QtGui.QLabel(self.tab_3)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_2.addWidget(self.label_3)
        self.txtAvg = QtGui.QLineEdit(self.tab_3)
        self.txtAvg.setMaximumSize(QtCore.QSize(100, 16777215))
        self.txtAvg.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.txtAvg.setReadOnly(True)
        self.txtAvg.setObjectName(_fromUtf8("txtAvg"))
        self.horizontalLayout_2.addWidget(self.txtAvg)
        self.label_4 = QtGui.QLabel(self.tab_3)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_2.addWidget(self.label_4)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.tblStatus = QtGui.QTableWidget(self.tab_3)
        font = QtGui.QFont()
        font.setPointSize(8)
        self.tblStatus.setFont(font)
        self.tblStatus.setInputMethodHints(QtCore.Qt.ImhNone)
        self.tblStatus.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.tblStatus.setRowCount(50)
        self.tblStatus.setColumnCount(16)
        self.tblStatus.setObjectName(_fromUtf8("tblStatus"))
        item = QtGui.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(False)
        font.setWeight(75)
        item.setFont(font)
        self.tblStatus.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(False)
        font.setWeight(75)
        item.setFont(font)
        self.tblStatus.setHorizontalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(5, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(6, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(7, item)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(False)
        font.setWeight(75)
        item.setFont(font)
        self.tblStatus.setHorizontalHeaderItem(8, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(9, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(10, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(11, item)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(False)
        font.setWeight(75)
        item.setFont(font)
        self.tblStatus.setHorizontalHeaderItem(12, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(13, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(14, item)
        item = QtGui.QTableWidgetItem()
        self.tblStatus.setHorizontalHeaderItem(15, item)
        self.tblStatus.horizontalHeader().setVisible(True)
        self.tblStatus.horizontalHeader().setDefaultSectionSize(70)
        self.tblStatus.verticalHeader().setVisible(False)
        self.tblStatus.verticalHeader().setDefaultSectionSize(16)
        self.verticalLayout_2.addWidget(self.tblStatus)
        self.tabWidget.addTab(self.tab_3, _fromUtf8(""))
        self.tab_4 = QtGui.QWidget()
        self.tab_4.setObjectName(_fromUtf8("tab_4"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.tab_4)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.txtLog = QtGui.QTextEdit(self.tab_4)
        self.txtLog.setAutoFillBackground(False)
        self.txtLog.setReadOnly(True)
        self.txtLog.setObjectName(_fromUtf8("txtLog"))
        self.verticalLayout_3.addWidget(self.txtLog)
        self.tabWidget.addTab(self.tab_4, _fromUtf8(""))
        self.verticalLayout.addWidget(self.tabWidget)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Listener", None))
        self.label.setText(_translate("MainWindow", "Start Port (default:1000) = ", None))
        self.txtPort.setText(_translate("MainWindow", "10000", None))
        self.btnCreate.setText(_translate("MainWindow", "Create Servers", None))
        self.label_2.setText(_translate("MainWindow", "Total = ", None))
        self.label_3.setText(_translate("MainWindow", "kb.     Speed = ", None))
        self.label_4.setText(_translate("MainWindow", " kb/s", None))
        item = self.tblStatus.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Offset", None))
        item = self.tblStatus.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Total(B)", None))
        item = self.tblStatus.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Speed(B/s)", None))
        item = self.tblStatus.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Status", None))
        item = self.tblStatus.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "Offset", None))
        item = self.tblStatus.horizontalHeaderItem(5)
        item.setText(_translate("MainWindow", "Total(B)", None))
        item = self.tblStatus.horizontalHeaderItem(6)
        item.setText(_translate("MainWindow", "Speed(B/s)", None))
        item = self.tblStatus.horizontalHeaderItem(7)
        item.setText(_translate("MainWindow", "Status", None))
        item = self.tblStatus.horizontalHeaderItem(8)
        item.setText(_translate("MainWindow", "Offset", None))
        item = self.tblStatus.horizontalHeaderItem(9)
        item.setText(_translate("MainWindow", "Total(B)", None))
        item = self.tblStatus.horizontalHeaderItem(10)
        item.setText(_translate("MainWindow", "Speed(B/s)", None))
        item = self.tblStatus.horizontalHeaderItem(11)
        item.setText(_translate("MainWindow", "Status", None))
        item = self.tblStatus.horizontalHeaderItem(12)
        item.setText(_translate("MainWindow", "Offset", None))
        item = self.tblStatus.horizontalHeaderItem(13)
        item.setText(_translate("MainWindow", "Total(B)", None))
        item = self.tblStatus.horizontalHeaderItem(14)
        item.setText(_translate("MainWindow", "Speed(B/s)", None))
        item = self.tblStatus.horizontalHeaderItem(15)
        item.setText(_translate("MainWindow", "Status", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("MainWindow", "Diagnostic", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), _translate("MainWindow", "Logs", None))

