#!/usr/bin/env bash
# Creates python files from .ui files
pyuic4 listener_gui_design.ui -o listener_gui_design.py
pyuic4 sender_gui_design.ui -o sender_gui_design.py
