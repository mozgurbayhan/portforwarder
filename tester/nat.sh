#!/bin/sh
# FOR TESTING NAT OVER IPTABLES
echo "Flushing previous rules..."
iptables -F
iptables -t nat -F
iptables -X

echo "Allowing ip forward..."
echo 1 > /proc/sys/net/ipv4/ip_forward

echo "adding routing rules"
INTWAN=enp2s0
INTLAN=enp2s8
iptables -t nat -A POSTROUTING -o ${INTLAN} -j MASQUERADE
iptables -A FORWARD -i ${INTLAN} -o ${INTWAN} -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i ${INTWAN} -o ${INTLAN} -j ACCEPT

# echo "Adding port forward rules..."

# iptables -t nat -A PREROUTING -p TCP --dport 10000 -j DNAT --to-destination 10.1.1.5:30000
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.5 --dport 30000 -j SNAT --to-source 192.168.0.142
# iptables -t nat -A PREROUTING -p TCP --dport 10001 -j DNAT --to-destination 10.1.1.5:30001
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.5 --dport 30001 -j SNAT --to-source 192.168.0.142
# iptables -t nat -A PREROUTING -p TCP --dport 8000 -j DNAT --to-destination 10.1.1.5:80
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.5 --dport 80 -j SNAT --to-source 192.168.0.142

#iptables -t nat -A PREROUTING -p TCP -i enp2s8 --dport 8000 -j DNAT --to-destination 10.1.1.5:80
##iptables -A FORWARD -p tcp -d 10.1.1.5 --dport 80 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
#iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.5 --dport 80 -j SNAT --to-source 192.168.0.142

# iptables -t nat -A PREROUTING -p TCP --dport 10002 -j DNAT --to-destination 10.1.1.9:30000
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.9 --dport 30000 -j SNAT --to-source 192.168.0.142
# iptables -t nat -A PREROUTING -p TCP --dport 10003 -j DNAT --to-destination 10.1.1.9:30001
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.9 --dport 30001 -j SNAT --to-source 192.168.0.142
# iptables -t nat -A PREROUTING -p TCP --dport 8001 -j DNAT --to-destination 10.1.1.9:80
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.9 --dport 80 -j SNAT --to-source 192.168.0.142
# 
# iptables -t nat -A PREROUTING -p TCP --dport 10004 -j DNAT --to-destination 10.1.1.13:30000
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.13 --dport 30000 -j SNAT --to-source 192.168.0.142
# iptables -t nat -A PREROUTING -p TCP --dport 10005 -j DNAT --to-destination 10.1.1.13:30001
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.13 --dport 30001 -j SNAT --to-source 192.168.0.142
# iptables -t nat -A PREROUTING -p TCP --dport 8002 -j DNAT --to-destination 10.1.1.13:80
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.13 --dport 80 -j SNAT --to-source 192.168.0.142
# 
# iptables -t nat -A PREROUTING -p TCP --dport 10006 -j DNAT --to-destination 10.1.1.17:30000
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.17 --dport 30000 -j SNAT --to-source 192.168.0.142
# iptables -t nat -A PREROUTING -p TCP --dport 10007 -j DNAT --to-destination 10.1.1.17:30001
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.17 --dport 30001 -j SNAT --to-source 192.168.0.142
# iptables -t nat -A PREROUTING -p TCP --dport 8003 -j DNAT --to-destination 10.1.1.17:80
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.1.17 --dport 80 -j SNAT --to-source 192.168.0.142
# 
# iptables -t nat -A PREROUTING -p TCP --dport 10008 -j DNAT --to-destination 10.1.2.5:30000
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.2.5 --dport 30000 -j SNAT --to-source 192.168.0.142
# iptables -t nat -A PREROUTING -p TCP --dport 10009 -j DNAT --to-destination 10.1.2.5:30001
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.2.5 --dport 30001 -j SNAT --to-source 192.168.0.142
# iptables -t nat -A PREROUTING -p TCP --dport 8004 -j DNAT --to-destination 10.1.2.5:80
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.2.5 --dport 80 -j SNAT --to-source 192.168.0.142

## For Test
# iptables -t nat -A PREROUTING -p TCP --dport 8888 -j DNAT --to-destination 10.1.3.5:80
# iptables -t nat -A POSTROUTING -p TCP -d 10.1.2.5 --dport 80 -j SNAT --to-source 192.168.0.142

echo "Done!"
