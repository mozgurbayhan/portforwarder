# -*- coding: utf-8 -*-
import socket
from PyQt4 import QtCore
from time import sleep, time

from PyQt4.QtCore import QTimer
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QColor
from PyQt4.QtGui import QMainWindow
from PyQt4.QtGui import QTableWidgetItem

from common import PACKAGE_SIZE, TARGET_IP, PACKAGE, CONNECTION_TYPE, TCP, UDP
from sender_gui_design import Ui_MainWindow

__author__ = 'ozgur'
__creation_date__ = '24.02.2017' '10:26'


# noinspection PyPep8Naming,PyMethodMayBeStatic
class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(self.__class__, self).__init__()

        self.setupUi(self)
        self.worker_list = []
        self.total_byte_list = []
        self.temp_byte_list = []
        self._populate_table()
        self.lblPacksize.setText("x %dB" % (PACKAGE_SIZE,))

        self.timerHarvest = QTimer()
        self.timerHarvest.timeout.connect(self.event_timerHarvest_tick)
        self.timerHarvest.start(2000)

        self.btnSet.clicked.connect(self.event_btnSet_clicked)
        self.btnUnset.clicked.connect(self.event_btnUnset_clicked)

    def _get_start_cell(self, conn_id):
        x = conn_id % 50
        y = (conn_id / 50) * 4
        return x, y

    def _populate_table(self):
        # w_oldoffset = QTableWidgetItem(str(raw_diff_list[i][0]))
        # w_oldoffset.setBackground(rcolor)
        for socketnum in range(200):
            x, y = self._get_start_cell(socketnum)
            w_cellheader = QTableWidgetItem(str(socketnum))
            w_cellheader.setBackgroundColor(QColor(220, 220, 220))
            w_cellheader.setTextAlignment(Qt.AlignCenter)
            self.tblStatus.setItem(x, y, w_cellheader)
            self.total_byte_list.append(0)
            self.temp_byte_list.append(0)
            self.worker_list.append(None)

    def _get_txt_widget_values(self):
        # BASE PORT
        self.txtPort.setEnabled(False)
        try:
            port_start = int(self.txtPort.text())
        except ValueError:
            port_start = 10000
        if port_start > 60000 or port_start < 1000:
            port_start = 10000
        self.txtPort.setText(str(port_start))

        # PACKAGE SIZE
        try:
            packsize = int(self.txtPackSize.text())
        except ValueError:
            packsize = 1
        self.txtPackSize.setText(str(packsize))

        # OFFSET VALUES
        offset = str(self.txtOffset.text())
        olist = offset.replace(" ", "").split("-")
        try:
            start = int(olist[0])
        except ValueError:
            start = 0

        try:
            end = int(olist[1])
        except ValueError:
            end = None
        except IndexError:
            end = None
        if end is not None and end > 199:
            end = 199
        if start < 0:
            start = 0

        if end is None:
            self.txtOffset.setText(str(start))
        else:
            self.txtOffset.setText(str(start) + "-" + str(end))
        return port_start, start, end, packsize

    def set_cell_value(self, x, y, value, is_status=False):

        if is_status:
            if value:
                wdata = QTableWidgetItem("CONN")
                wdata.setBackgroundColor(QColor(220, 255, 220))
            else:
                wdata = QTableWidgetItem("N/A")
                wdata.setBackgroundColor(QColor(255, 220, 220))
        else:
            wdata = QTableWidgetItem(str(value))
        wdata.setTextAlignment(Qt.AlignCenter)
        self.tblStatus.setItem(x, y, wdata)

    def _finalize_worker(self, worker_id):
        if self.worker_list[worker_id] is not None:
            self.worker_list[worker_id].kill_socket()
            self.worker_list[worker_id].terminate()
            self.worker_list[worker_id] = None

    def set_workers(self, packsize, port_start, start, end=None):
        self.unset_workers(start, end)

        if end is None:
            end = start
        end += 1

        sleep(0.01)
        for x in range(start, end):
            worker = QtWorkerSender(packsize, port_start, x)
            worker.signal_bytes_sent.connect(self.slot_bytes_sent)
            worker.signal_end_connection.connect(self.slot_end_connection)
            worker.signal_new_connection.connect(self.slot_new_connection)
            worker.start()
            self.worker_list[x] = worker

    def unset_workers(self, start, end=None):
        if end is None:
            end = start

        end += 1
        for x in range(start, end):
            self._finalize_worker(x)

    def event_btnSet_clicked(self):
        port_start, start, end, packsize = self._get_txt_widget_values()
        self.set_workers(packsize, port_start, start, end=end)

    def event_btnUnset_clicked(self):
        port_start, start, end, packsize = self._get_txt_widget_values()
        self.unset_workers(start, end=end)

    def event_timerHarvest_tick(self):
        try:
            total = 0
            avg = 0
            for i in range(200):
                temp_data = self.temp_byte_list[i]
                self.temp_byte_list[i] = 0
                total_data = self.total_byte_list[i] = self.total_byte_list[i] + temp_data
                x, y = self._get_start_cell(i)

                self.set_cell_value(x, y + 1, total_data, is_status=False)
                self.set_cell_value(x, y + 2, temp_data / 2, is_status=False)
                total += total_data
                avg += temp_data
                self.txtAvg.setText(str(avg / (1024 * 2)))
                self.txtTotal.setText(str(total / 1024))
        except IndexError:
            pass

    def slot_new_connection(self, conn_id, port_offset):
        self.txtLog.append("Set connection = " + str(conn_id) + ",port: " + str(port_offset))
        x, y = self._get_start_cell(conn_id)
        self.set_cell_value(x, y + 3, True, is_status=True)

    def slot_end_connection(self, conn_id, port_offset):
        self.txtLog.append("End connection = " + str(conn_id) + ",port: " + str(port_offset))
        x, y = self._get_start_cell(conn_id)
        self.set_cell_value(x, y + 3, False, is_status=True)

    def slot_bytes_sent(self, conn_id, port_offset, byte_count):
        self.temp_byte_list[conn_id] = self.temp_byte_list[conn_id] + byte_count


class QtWorkerSender(QtCore.QThread):
    signal_new_connection = QtCore.pyqtSignal(object, object)
    signal_end_connection = QtCore.pyqtSignal(object, object)
    signal_bytes_sent = QtCore.pyqtSignal(object, object, object)

    def __init__(self, packsize, port_offset, conn_id):
        QtCore.QThread.__init__(self)
        self.packsize = packsize
        self.port = port_offset + conn_id
        self.conn_id = conn_id
        self.sock = None
        self.ip = TARGET_IP
        self.buffer_size = PACKAGE_SIZE
        self.alive = True

        self.bigpackage = ""
        for i in range(self.packsize):
            self.bigpackage += PACKAGE

    def send_tcp(self):
        self.alive = True
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            bpsize = len(self.bigpackage)
            self.sock.connect((self.ip, self.port))
            self.signal_new_connection.emit(self.conn_id, self.port)
            while self.alive:
                tc = time()
                self.signal_bytes_sent.emit(self.conn_id, self.port, bpsize)
                self.sock.send(self.bigpackage)
                tdiff = time() - tc
                if tdiff < 1:
                    sleep(1.00 - tdiff)
        except socket.error:
            pass

        self.signal_end_connection.emit(self.conn_id, self.port)

    def send_udp(self):
        self.alive = True
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            bpsize = len(self.bigpackage)
            self.signal_new_connection.emit(self.conn_id, self.port)
            while self.alive:
                tc = time()
                for i in range(self.packsize):
                    self.sock.sendto(PACKAGE, (self.ip, self.port))
                self.signal_bytes_sent.emit(self.conn_id, self.port, bpsize)
                tdiff = time() - tc
                if tdiff < 1:
                    sleep(1.00 - tdiff)
        except socket.error:
            pass

        self.signal_end_connection.emit(self.conn_id, self.port)

    def run(self):

        if CONNECTION_TYPE == TCP:
            self.send_tcp()
        elif CONNECTION_TYPE == UDP:
            self.send_udp()
        else:
            pass

    def kill_socket(self):
        self.alive = False
        self.sock.close()
        self.signal_end_connection.emit(self.conn_id, self.port)
