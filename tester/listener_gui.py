# -*- coding: utf-8 -*-
import socket
from PyQt4 import QtCore
from time import sleep

from PyQt4.QtCore import QTimer
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QColor
from PyQt4.QtGui import QMainWindow
from PyQt4.QtGui import QTableWidgetItem

from common import PACKAGE_SIZE, CONNECTION_TYPE, TCP, UDP
from listener_gui_design import Ui_MainWindow

__author__ = 'ozgur'
__creation_date__ = '23.02.2017' '14:56'


# noinspection PyPep8Naming,PyMethodMayBeStatic
class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(self.__class__, self).__init__()

        self.setupUi(self)

        self.timerHarvest = QTimer()
        self.timerHarvest.timeout.connect(self.event_timerHarvest_tick)
        self.timerHarvest.start(2000)

        self.worker_list = []
        self.total_byte_list = []
        self.temp_byte_list = []
        self._populate_table()

        self.btnCreate.clicked.connect(self.event_btnCreate_clicked)

    def _initialize_workers(self):
        '''
        initializes listener workers\n
        :return:
        '''
        # self.qt_worker_diff_maker = QtWorkerDiffMaker(oldfilepath, newfilepath, savepath, debug)
        # self.qt_worker_diff_maker.signal_process_started.connect(self.slot_process_started)
        # self.qt_worker_diff_maker.signal_step_started.connect(self.slot_step_started)
        try:
            port_start = int(self.txtPort.text())
        except ValueError:
            port_start = 10000
        if port_start > 60000 or port_start < 1000:
            port_start = 10000

        self.txtPort.setText(str(port_start))
        for worker in self.worker_list:
            try:
                worker.kill_socket()
                sleep(0.2)
                worker.terminate()
            except:
                pass

        for x in range(200):
            worker = QtWorkerListener(x, port_start)
            worker.signal_bytes_recieved.connect(self.slot_bytes_recieved)
            worker.signal_end_connection.connect(self.slot_end_connection)
            worker.signal_new_connection.connect(self.slot_new_connection)
            worker.signal_server_initialized.connect(self.slot_server_initialized)
            worker.start()
            self.worker_list.append(worker)
            self.total_byte_list.append(0)
            self.temp_byte_list.append(0)

        pass

    def _get_start_cell(self, conn_id):
        '''
        returns first cell of given connection id
        :param conn_id: int
        :return:
        '''
        x = conn_id % 50
        y = (conn_id / 50) * 4
        return x, y

    def _populate_table(self):
        '''
        fills gui table \n
        :return:
        '''
        # w_oldoffset = QTableWidgetItem(str(raw_diff_list[i][0]))
        # w_oldoffset.setBackground(rcolor)
        for socketnum in range(200):
            x, y = self._get_start_cell(socketnum)
            w_cellheader = QTableWidgetItem(str(socketnum))
            w_cellheader.setBackgroundColor(QColor(220, 220, 220))
            w_cellheader.setTextAlignment(Qt.AlignCenter)
            self.tblStatus.setItem(x, y, w_cellheader)

    def set_cell_value(self, x, y, value, is_status=False):
        '''
        sets cell's value
        :param x: int
        :param y: int
        :param value:
        :param is_status: bool
        :return:
        '''
        if is_status:
            if value:
                wdata = QTableWidgetItem("CONN")
                wdata.setBackgroundColor(QColor(220, 255, 220))
            else:
                wdata = QTableWidgetItem("N/A")
                wdata.setBackgroundColor(QColor(255, 220, 220))
        else:
            wdata = QTableWidgetItem(str(value))
        wdata.setTextAlignment(Qt.AlignCenter)
        self.tblStatus.setItem(x, y, wdata)

    def slot_server_initialized(self, conn_id, port_offset):
        self.txtLog.append("Listener Started : id = " + str(conn_id) + ",port: " + str(port_offset))
        x, y = self._get_start_cell(conn_id)
        self.set_cell_value(x, y + 3, False, is_status=True)

        # w_cellheader = QTableWidgetItem(str(socketnum))

    def slot_new_connection(self, conn_id, port_offset):
        self.txtLog.append("Get connection = " + str(conn_id) + ",port: " + str(port_offset))
        x, y = self._get_start_cell(conn_id)
        self.set_cell_value(x, y + 3, True, is_status=True)

    def slot_end_connection(self, conn_id, port_offset):
        self.txtLog.append("Lost connection = " + str(conn_id) + ",port: " + str(port_offset))
        x, y = self._get_start_cell(conn_id)
        self.set_cell_value(x, y + 3, False, is_status=True)

    def slot_bytes_recieved(self, conn_id, port_offset, byte_count):
        self.temp_byte_list[conn_id] = self.temp_byte_list[conn_id] + byte_count

    def event_btnCreate_clicked(self):
        self._initialize_workers()
        self.btnCreate.setDisabled(True)
        self.txtPort.setDisabled(True)

    def event_timerHarvest_tick(self):
        try:
            total = 0
            avg = 0
            for i in range(200):
                temp_data = self.temp_byte_list[i]
                self.temp_byte_list[i] = 0
                total_data = self.total_byte_list[i] = self.total_byte_list[i] + temp_data
                x, y = self._get_start_cell(i)

                self.set_cell_value(x, y + 1, total_data, is_status=False)
                self.set_cell_value(x, y + 2, temp_data / 2, is_status=False)
                total += total_data
                avg += temp_data
            self.txtAvg.setText(str(avg / (1024 * 2)))
            self.txtTotal.setText(str(total / 1024))
        except IndexError:
            pass


class QtWorkerListener(QtCore.QThread):
    signal_server_initialized = QtCore.pyqtSignal(object, object)
    signal_new_connection = QtCore.pyqtSignal(object, object)
    signal_end_connection = QtCore.pyqtSignal(object, object)
    signal_bytes_recieved = QtCore.pyqtSignal(object, object, object)

    def __init__(self, conn_id, port_offset):
        QtCore.QThread.__init__(self)
        self.port = port_offset + conn_id
        self.conn_id = conn_id
        self.ip = '0.0.0.0'
        self.buffer_size = PACKAGE_SIZE
        self.sock = None
        self.conn = None

    def listen_tcp(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((self.ip, self.port))
        self.signal_server_initialized.emit(self.conn_id, self.port)
        self.sock.listen(1)
        while True:
            self.conn, addr = self.sock.accept()
            self.signal_new_connection.emit(self.conn_id, self.port)
            while True:
                data = self.conn.recv(self.buffer_size)
                if not data or data == "":
                    self.signal_end_connection.emit(self.conn_id, self.port)
                    break
                self.signal_bytes_recieved.emit(self.conn_id, self.port, self.buffer_size)
            self.conn.close()

    def listen_udp(self):
        has_conn = False
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((self.ip, self.port))
        self.signal_server_initialized.emit(self.conn_id, self.port)
        while True:
            data, addr = self.sock.recvfrom(self.buffer_size)
            if not has_conn:
                self.signal_new_connection.emit(self.conn_id, self.port)
                has_conn = True
            self.signal_bytes_recieved.emit(self.conn_id, self.port, len(data))

    def run(self):
        if CONNECTION_TYPE == TCP:
            self.listen_tcp()
        elif CONNECTION_TYPE == UDP:
            self.listen_udp()
        else:
            pass
