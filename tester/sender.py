# -*- coding: utf-8 -*-
import sys
from PyQt4.QtGui import QApplication
from sender_gui import MainWindow

__author__ = 'ozgur'
__creation_date__ = '24.02.2017' '10:25'


def main():
    # APPLICATION
    app = QApplication(sys.argv)  # A new instance of QApplication
    form = MainWindow()  # We set the form to be our ExampleApp (design)
    form.show()  # Show the form
    app.exec_()  # and execute the app


if __name__ == '__main__':  # if we're running file directly and not importing it
    main()  # run the main function
